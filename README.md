ณัฎฐพัชร์ ปุณณหิรัญย์

=>  อธิบาย Commit
#<COMMIT - e605cc9>
- สร้างไฟล์ project และเพิ่ม maven เข้า project
-เพิ่ม .gitignore และสร้างไฟล์ README.md

#<COMMIT - 31e5af4>
- สร้าง class Home /HomeControllers, class Admin /AdminControllers
- สร้างหน้าของ home, admin ใน  ScenceBuilder

#<COMMIT - d96423f>
- สร้าง class GeneralWork/GeneralControllers, class Weekwork/WeekWorkControllers, 
class ForwardWork/ForwardWorkControllers, class ProjectWork/ProjectWorkControllers,
class Menu/MenuControllers, class Type,TypeControllers
- สร้างหน้าของ generalwork, weekwork, forwardwork, projectwork, menu, type ใน  ScenceBuilder
- ทำเขียนอ่านไฟล์ของ class Type (หมวดหมู่) และเพิ่มปุ่มช้อยส์ให้เลือกหมวดหมู๋ได้ในแต่ละงาน และสร้างไฟล์ typeData.csv
- นำ inheritance มาใช้กับ class โดยให้ weekwork, projectwork สืบทอด generalwork และให้ forwardwork 
สืบทอด projectwork

#<COMMIT - 1bc1fcf>
-สร้าง class Work ขึ้นมาเพื่อให้ generalwork สืบทอดอีกที เพื่อการ polymophism method file_toString
-ทำเขียนอ่านไฟล์ของ class GeneralWork, class Weekwork, class ForwardWork, class ProjectWork
และสร้างไฟล์ workAll.csv 

#<COMMIT - 3f8b3b3>
- ลบ class Work ทิ้ง แล้วให้ทุก class มาสืบทอด class General
- แก้ไข constructor ของทุก class และแก้ super ของ class Weekwork, class ForwardWork, class ProjectWork
- เปลี่ยนชื่อ filed ใน class ForwardWork จาก list_leader เป็น name_res
- แก้ไข index ใน method อ่านไฟล์ 

#<COMMIT - 058b431>
- สร้าง class ListOfWorkControllers และ class EditofGeneralWorkControllers, 
class EditWeekworkControllers, class EditForwardWorkControllers, 
class EditProjectWorkControllers
- สร้างหน้า List และหน้าแก้ไขงานของทั้ง 4 งาน ใน  ScenceBuilder
- สร้างหน้าเมนู  ใน  ScenceBuilder
- สร้างหน้า instruction ใน ScenceBuilder
- เพิ่ม method คำนวณหมวดหมู่ว่ามีกี่งาน ใน class EverythingCheck 

#<COMMIT - 4bca7eb>
- แก้โค้ดที่มีคำว่า commit มาแทรกในโค้ด

#<COMMIT - eb2ac27>
- เพิ่ม method คำนวณหมวดหมู่ในงานแต่ละประเภท ใน class EverythingCheck
- จัดความเรียบร้อยของชื่อไฟล์
- ตกแต่งโปรแกรม
- เพิ่มโฟลเดอร์ WorkManagement

#<COMMIT - 7037270>
- Add ทุกอย่างที่เป็น jar file 

#<COMMIT - da6fbfc>  
- แก้คำผิดในหน้า ScenceBuilder ของแต่ละงาน
- push 6210450806.jar

#<COMMIT - 43fd650> 
- แก้ไขไฟล์ pdf



==> การวางโครงสร้างไฟล์
#####* src/main/java/work/Models/GeneralWork
- เก็บ Class ของงานทั่วไป คือ ชื่องาน วันที่ทำ เวลาที่เริ่มทำ เวลาที่สิ้นสุดการทำ ลำดับความสำคัญ สถานะของงาน
#####* src/main/java/work/Models/WeekWork
- เก็บ Class ของงานประจำสัปดาห์ คือ ชื่องาน วันประจำสัปดาห์ที่ทำ เวลาที่เริ่มทำ เวลาที่สิ้นสุดการทำ ลำดับความสำคัญ สถานะของงาน
#####* src/main/java/work/Models/ForwardWork
- เก็บ Class ของงานส่งต่อ คือ ชื่องาน ชื่อผู้รับผิดชอบงานส่งต่อ วันที่ทำ เวลาที่เริ่มทำ ลำดับความสำคัญ สถานะของงาน
#####* src/main/java/work/Models/ProjectWork
- เก็บ Class ของงานโครงการ คือ ชื่องาน ชื่อหัวหน้าโครงการ วันที่เริ่มทำ วันที่สิ้นสุดการทำ ลำดับความสำคัญ สถานะของงาน
#####* src/main/java/work/Models/Type
- เก็บ Class ของหมวดหมู่ คือ ชื่อหมวดหมู่
#####* src/main/java/work/AllUse/EverythingCheck
- เก็บ Class ของ Method ที่ใช้เป็นเงื่อนไขต่างๆของงาน
#####* src/main/java/work/AllUse/RWfile
- เก็บ Class ของ อ่านเขียนไฟล์ของงาน และหมวดหมู่


#####* src/main/java/work/Controllers/Create/CreatingGeneralWorkControllers
- หน้าของการสร้างงานทั่วไป
#####* src/main/java/work/Controllers/Create/CreatingWeekWorkControllers
- หน้าของการสร้างงานประจำสัปดาห์
#####* src/main/java/work/Controllers/Create/CreatingForwardWorkControllers
- หน้าของการสร้างงานส่งต่อ
#####* src/main/java/work/Controllers/Create/CreatingProjectWorkControllers
- หน้าของการสร้างงานโครงการ
#####* src/main/java/work/Controllers/Create/CreatingTypeControllers
- หน้าของการสร้างหมวดหมู่


#####* src/main/java/work/Controllers/Edit/EditGeneralWorkControllers
- หน้าของการแก้ไขงานทั่วไป
#####* src/main/java/work/Controllers/Edit/EditWeekWorkControllers
- หน้าของการแก้ไขงานประจำทั่วไป
#####* src/main/java/work/Controllers/Edit/EditForwardWorkControllers
- หน้าของการแก้ไขงานส่งต่อ
#####* src/main/java/work/Controllers/Edit/EditProjectWorkControllers
- หน้าของการแก้ไขงานโครงการ


#####* src/main/java/work/Controllers/HomeControllers
- หน้าแรกของโปรแกรม
#####* src/main/java/work/Controllers/InstructionControllers
- หน้าแนะนำการใช้โปรแกรม
#####* src/main/java/work/Controllers/ListOfWorkAllControllers
- หน้ารายการงานทั่งหมด
#####* src/main/java/work/Controllers/ListOfWorkAllControllers
- หน้าเมนูรวม โดยให้เลือกระหว่าง สร้างงานและสร้างหมวดหมู่ หรือ รายการของงาน
#####* src/main/java/work/Controllers/MenuOfCreatingControllers
- หน้าเมนูของการสร้างงาน ว่าจะสร้างงานทั่วไป งานประจำสัปดาห์ งานส่งต่อ หรืองานโครงการ และสร้างหมวดหมู่
#####* src/main/java/work/Controllers/ProducerControllers
- หน้าของผู้จัดทำ


####* src/main/resources
- เก็บไฟล์ fxml ทั้งหมด
####* src/main/iconCute
- เก็บไฟล์รูปที่ใช้ตกแต่งโปรแกรม




==> วิธีติดตั้ง Font
- เข้าโฟลเดอร์ Fonts แล้วติดตั้ง ROCC____.TTF,ROCCB___.TFF,ROCCB___.TFF,ROCKB.TFF,ROCKBI.TFF,ROCKEB.TFF,ROCKI.TFF



==> วิธี run โปรแกรม
** : Window :**
- เข้าโฟล์เดอร์ Work_Management -> double click 6210450806.jar

** : Mac OS :**
- เข้าโฟล์เดอร์ Work_Management -> คลิกขวาเปิด 6210450806.jar



==> วิธีใช้งานโปรแกรมเบื้องต้น
เมื่อเปิดโปรแกรมขึ้นมา ทำการกดปุ่ม Menu ในหน้านั้นจะมีปุ่มให้เลือกระหว่าง สร้างงาน/สร้างหมวดหมู่ของงาน
และปุ่มแสดงรายการของงานทั้งหมด

โดยถ้าเเลือก Creating Work and Category
จะแบ่งเป็น 4 งาน คือ General Work, Week Work, Forward Work, Project Work
ถ้าผู้ใช้เลือกมา 1 งาน
1.) กรอกข้อมูลให้ครบ แล้วกดปุ่มค CONFIRM
2.) ชื่องาน ต้องเป็นภาษาอังกฤษ ห้ามซ้ำกับชื่องานเดิม
3.) การกรอกเวลา ต้องกรอกแบบสากล เช่น 12:00pm เป็นต้น

และถ้าเลือก List of work and Edit of Work
หน้า ListView จะแสดงรายการของงานทั้งหมด จะมีช้อยส์ให้เลือก 3 ช้อยส์
คือ Category, Status, Date โดยไม่ต้องเลือกให้ครบก็ได้ 
แล้วกดปุ่ม Show ได้เลย โดยแต่ละช้อยส์จะแสดงรายการงานที่มีข้อมูลตรงกับที่เลือกไป
หมายเหตุ : Date ต้องใช้วันที่เริ่มทำ แต่ Project Work (งานโครงการ) ต้องใช้วันที่จบการการทำงานใน Date

วิธีดูจำนวนหมวดหมู่ และงานในแต่ละประเภทในหมวดหมู่นั้น
1.) เลือกหมวดหมู่ใดหมวดหมู่หนึ่งใน Category
2.) กดปุ่ม Show

วิธีดูสถานะของงาน
1.) เลือกสถานะใดสถานะหนึ่งใน Status
2,) กดปุ่ม Show

วิธีดู Information เพิ่มเติมของงาน
1.) กดที่รายการใดรายการหนึ่งของงาน
2.) กดปุ่ม Information

วิธีทำการแก้ไขงาน
1.) กดที่รายการใดรายการหนึ่งของงานที่จะแก้
2.) กดปุ่ม Edit of work 
ซึ่งการแก้ไขงานนั้น ก็ใช้วิธีเดียวกับการสร้างงาน คือ
1.) กรอกข้อมูลให้ครบ แล้วกดปุ่มค CONFIRM
2.) ชื่องาน ต้องเป็นภาษาอังกฤษ ห้ามซ้ำกับชื่องานเดิม
3.) การกรอกเวลา ต้องกรอกแบบสากล เช่น 12:00pm เป็นต้น