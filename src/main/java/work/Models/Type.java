package work.Models;

public class Type {

    private String nameType;

    public Type() {
    }

    public Type(String nameType) {

        this.nameType = nameType;
    }

    public void setNameType(String nameType) {

        this.nameType = nameType;
    }

    public String getNameType() {
        return nameType;
    }
}
