package work.Models;

public class GeneralWork {
    private String name_work;
    private String day;
    private String importance;
    private String status;
    private String start;
    private String end;
    private String type;
    private String type_category;

    public GeneralWork() {
    }

    public GeneralWork(String importance, String status, String type, String type_category) {
        this.importance = importance;
        this.status = status;
        this.type_category = type_category;
        this.type = type;
    }

    public GeneralWork(String name_work, String day, String importance, String status, String start, String end, String type, String type_category) {
        this.name_work = name_work;
        this.day = day;
        this.importance = importance;
        this.status = status;
        this.start = start;
        this.end = end;
        this.type = type;
        this.type_category = type_category;
    }

    public GeneralWork(String importance, String day, String start, String status, String type, String type_category) {
        this.importance = importance;
        this.day = day;
        this.start = start;
        this.status = status;
        this.type = type;
        this.type_category = type_category;
    }

    public String getName_work() {
        return this.name_work;
    }

    public String getDay() {
        return this.day;
    }

    public String getImportance() {
        return this.importance;
    }

    public String getStatus() {
        return this.status;
    }

    public String getStart() {
        return this.start;
    }

    public String getEnd() {
        return this.end;
    }

    public String getType() {
        return this.type;
    }

    public String getType_category() {
        return this.type_category;
    }

    public void setName_work(String name_work) {
        this.name_work = name_work;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public void setImportance(String importance) {
        this.importance = importance;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setType_category(String type_category) {
        this.type_category = type_category;
    }

    public String toString() {
        return "GeneralWork{name_work='" + this.name_work + '\'' + ", day='" + this.day + '\'' + ", importance='" + this.importance + '\'' + ", status='" + this.status + '\'' + ", start='" + this.start + '\'' + ", end='" + this.end + '\'' + ", category='" + this.type_category + '\'' + ", type='" + this.type + '\'' + '}';
    }

    public String file_toString() {
        return this.name_work + "," + this.day + "," + this.importance + "," + this.status + "," + this.start + "," + this.end + ",,,,,,,,,,," + this.type + "," + this.type_category;
    }
}
