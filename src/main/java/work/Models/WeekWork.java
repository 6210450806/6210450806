package work.Models;

public class WeekWork extends GeneralWork {
    private String wname_work;
    private String wday;
    private String wstart;
    private String wend;
    private String wtype;

    public WeekWork() {
    }

    public WeekWork(String importance, String status, String wname_work, String wday, String wstart, String wend, String wtype, String type_category) {
        super(importance, status, wtype, type_category);
        this.wname_work = wname_work;
        this.wday = wday;
        this.wstart = wstart;
        this.wend = wend;
        this.wtype = wtype;
    }

    public String getWname_work() {
        return this.wname_work;
    }

    public String getWday() {
        return this.wday;
    }

    public String getWstart() {
        return this.wstart;
    }

    public String getWend() {
        return this.wend;
    }

    public String getWtype() {
        return this.wtype;
    }

    public void setWname_work(String wname_work) {
        this.wname_work = wname_work;
    }

    public void setWday(String wday) {
        this.wday = wday;
    }

    public void setWstart(String wstart) {
        this.wstart = wstart;
    }

    public void setWend(String wend) {
        this.wend = wend;
    }

    public void setWtype(String wtype) {
        this.wtype = wtype;
    }

    public String toString() {
        return super.toString() + "WeekWork{wname_work='" + this.wname_work + '\'' + ", wday='" + this.wday + '\'' + ", wstart='" + this.wstart + '\'' + ", wend='" + this.wend + '\'' + ", wtype='" + this.wtype + '\'' + '}';
    }

    public String file_toString() {
        return this.wname_work + ",," + super.getImportance() + "," + super.getStatus() + ",,," + this.wname_work + "," + this.wday + "," + this.wstart + "," + this.wend + ",,,,,,," + super.getType() + "," + super.getType_category();
    }
}
