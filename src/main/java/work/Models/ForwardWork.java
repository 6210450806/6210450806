package work.Models;

public class ForwardWork extends GeneralWork {
    private String forward_name;
    private String name_res;
    private String type_forward;

    public ForwardWork() {
    }

    public ForwardWork(String day, String importance, String status, String start, String forward_name, String name_res, String type_category, String type_forward) {
        super(importance, day, start, status, type_forward, type_category);
        this.forward_name = forward_name;
        this.type_forward = type_forward;
        this.name_res = name_res;
    }

    public String getForward_name() {
        return this.forward_name;
    }

    public String getName_res() {
        return this.name_res;
    }

    public String getType_forward() {
        return this.type_forward;
    }

    public void setName_res(String name_res) {
        this.name_res = name_res;
    }

    public void setForward_name(String forward_name) {
        this.forward_name = forward_name;
    }

    public void setType_forward(String type_forward) {
        this.type_forward = type_forward;
    }

    public String toString() {
        return super.toString() + "ForwardWork{forward_name='" + this.forward_name + '\'' + ", name_res='" + this.name_res + '\'' + ", type_forward='" + this.type_forward + '\'' + '}';
    }

    public String file_toString() {
        return "," + super.getDay() + "," + super.getImportance() + "," + super.getStatus() + "," + super.getStart() + ",,,,,," + this.forward_name + "," + this.name_res + ",,,,," + super.getType() + "," + super.getType_category();
    }
}
