package work.Models;

public class ProjectWork extends GeneralWork {
    private String name_project;
    private String name_leader;
    private String day_start;
    private String day_end;
    private String type_project;

    public ProjectWork() {
    }

    public ProjectWork(String importance, String status, String type, String type_category, String name_project, String name_leader, String day_start, String day_end, String type_project) {
        super(importance, status, type, type_category);
        this.name_project = name_project;
        this.name_leader = name_leader;
        this.day_start = day_start;
        this.day_end = day_end;
        this.type_project = type_project;
    }

    public String getName_project() {
        return this.name_project;
    }

    public String getName_leader() {
        return this.name_leader;
    }

    public String getDay_start() {
        return this.day_start;
    }

    public String getDay_end() {
        return this.day_end;
    }

    public String getType_project() {
        return this.type_project;
    }

    public void setName_project(String name_project) {
        this.name_project = name_project;
    }

    public void setName_leader(String name_leader) {
        this.name_leader = name_leader;
    }

    public void setDay_start(String day_start) {
        this.day_start = day_start;
    }

    public void setDay_end(String day_end) {
        this.day_end = day_end;
    }

    public void setType_project(String type_project) {
        this.type_project = type_project;
    }

    public String toString() {
        return super.toString() + "ProjectWork{name_project='" + this.name_project + '\'' + ", name_leader='" + this.name_leader + '\'' + ", day_start='" + this.day_start + '\'' + ", day_end='" + this.day_end + '\'' + ", type_project='" + super.getType_category() + '\'' + '}';
    }

    public String file_toString() {
        return this.name_project + ",," + super.getImportance() + "," + super.getStatus() + ",,,,,,,,," + this.name_project + "," + this.name_leader + "," + this.day_start + "," + this.day_end + "," + super.getType() + "," + super.getType_category();
    }
}
