package work.AllUse;

import work.Models.*;

import java.io.*;
import java.util.ArrayList;

public class RWfile {

    public ArrayList<Type> readTypeDataBase() throws IOException {
        ArrayList<Type> types = new ArrayList();
        File file = new File("data/typeData.csv");
        FileReader fileReader = new FileReader(file);
        BufferedReader bufferedReader = new BufferedReader(fileReader);

        String line;
        for(int i = 0; (line = bufferedReader.readLine()) != null; ++i) {
            if (i != 0) {
                Type typeNew = new Type();
                typeNew.setNameType(line);
                types.add(typeNew);
            }
        }

        bufferedReader.close();
        return types;
    }

    public void writeTypeDataBase(ArrayList<Type> type) throws IOException {
        File file = new File("data/typeData.csv");
        FileWriter fileWriter = new FileWriter(file);
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

        for(int i = -1; i < type.size(); ++i) {
            if (i == -1) {
                bufferedWriter.write("nameType");
            } else {
                Type tName = type.get(i);
                bufferedWriter.write(tName.getNameType() + "");
            }

            bufferedWriter.newLine();
        }

        bufferedWriter.close();
    }

    public ArrayList<GeneralWork> readWorkDataBase() throws IOException {
        ArrayList<GeneralWork> work_list = new ArrayList();
        File file = new File("data/workAll.csv");
        FileReader fileReader = new FileReader(file);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String line;
        String[] colums;
        GeneralWork work_tmp;
        int i = 0;

        while (( line = bufferedReader.readLine()) != null) {
            if (i != 0) {
                colums = line.split(",");
                if (colums[16].equals("general")) {
                    work_tmp = new GeneralWork();
                    work_tmp.setName_work(colums[0]);
                    work_tmp.setDay(colums[1]);
                    work_tmp.setImportance(colums[2]);
                    work_tmp.setStatus(colums[3]);
                    work_tmp.setStart(colums[4]);
                    work_tmp.setEnd(colums[5]);
                    work_tmp.setType(colums[16]);
                    work_tmp.setType_category(colums[17]);
                    work_list.add(work_tmp);
                }

                if (colums[16].equals("week")) {
                    WeekWork work_week_tmp = new WeekWork();
                    work_week_tmp.setImportance(colums[2]);
                    work_week_tmp.setStatus(colums[3]);
                    work_week_tmp.setWname_work(colums[6]);
                    work_week_tmp.setName_work(colums[6]);
                    work_week_tmp.setWday(colums[7]);
                    work_week_tmp.setWstart(colums[8]);
                    work_week_tmp.setWend(colums[9]);
                    work_week_tmp.setDay(colums[7]);
                    work_week_tmp.setType(colums[16]);
                    work_week_tmp.setType_category(colums[17]);
                    work_list.add(work_week_tmp);
                }

                if (colums[16].equals("project")) {
                    ProjectWork projectwork_tmp = new ProjectWork();
                    projectwork_tmp.setName_project(colums[12]);
                    projectwork_tmp.setName_work(colums[12]);
                    projectwork_tmp.setName_leader(colums[13]);
                    projectwork_tmp.setImportance(colums[2]);
                    projectwork_tmp.setStatus(colums[3]);
                    projectwork_tmp.setDay_start(colums[14]);
                    projectwork_tmp.setDay_end(colums[15]);
                    projectwork_tmp.setDay(colums[15]);
                    projectwork_tmp.setType(colums[16]);
                    projectwork_tmp.setType_category(colums[17]);
                    work_list.add(projectwork_tmp);
                }

                if (colums[16].equals("forward")) {
                    ForwardWork forwardwork_tmp = new ForwardWork();
                    forwardwork_tmp.setDay(colums[1]);
                    forwardwork_tmp.setImportance(colums[2]);
                    forwardwork_tmp.setStatus(colums[3]);
                    forwardwork_tmp.setStart(colums[4]);
                    forwardwork_tmp.setForward_name(colums[10]);
                    forwardwork_tmp.setName_work(colums[10]);
                    forwardwork_tmp.setName_res(colums[11]);
                    forwardwork_tmp.setType(colums[16]);
                    forwardwork_tmp.setType_category(colums[17]);
                    work_list.add(forwardwork_tmp);
                }
            }
            i++;
        }

        bufferedReader.close();
        return work_list;
    }

    public void writeWorkDataBase(ArrayList<GeneralWork> works) throws IOException {
        File file = new File("data/workAll.csv");
        FileWriter fileWriter = new FileWriter(file);
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
        GeneralWork work;
        System.out.println((works.get(0)).getType());

        for(int i = -1; i < works.size(); ++i) {
            System.out.println(i);
            if (i == -1) {
                bufferedWriter.write("name_work,day,importance,status,start,end,wname_work,wday,wstart,wend,forward_name,name_res,name_project,name_leader,day_start,day_end,type,typecategory");
            } else {
                if ((works.get(i)).getType().equals("general")) {
                    work = works.get(i);
                    bufferedWriter.write(work.file_toString() + "");
                } else if ((works.get(i)).getType().equals("forward")) {
                    work = works.get(i);
                    bufferedWriter.write(work.file_toString() + "");
                } else if ((works.get(i)).getType().equals("week")) {
                    work = works.get(i);
                    bufferedWriter.write(work.file_toString() + "");
                } else if ((works.get(i)).getType().equals("project")) {
                    work = works.get(i);
                    bufferedWriter.write(work.file_toString() + "");
                }
            }

            bufferedWriter.newLine();
        }

        bufferedWriter.close();
    }
}
