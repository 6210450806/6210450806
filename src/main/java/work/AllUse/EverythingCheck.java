package work.AllUse;

import work.Models.GeneralWork;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EverythingCheck {

    public boolean checkNameType(String name){
        int Ascii;
        int flag = 0;
        int a = 97;
        int z = 122;
        int A = 65;
        int Z = 90;
        if (name.length() >= 2){
            for (int i=0 ; i<name.length() ; i++){
                Ascii = name.charAt(i);

                if (!((Ascii >= a && Ascii <= z) || (Ascii >= A && Ascii <= Z) ) && (Ascii != 32)){
                    flag = 1;
                    break;
                }
            }
        }
        else{
            flag = 1;
        }

        if (flag == 1){
            return false;
        }
        else {
            return true;
        }
    }

    public boolean checkTime(String time){
        Pattern VALID_TIME_REGEX =
                Pattern.compile("^(?:0?[0-9]|1[0-2])[-:][0-5][0-9]\\s*[ap]m$", Pattern.CASE_INSENSITIVE);

        Matcher matcher = VALID_TIME_REGEX.matcher(time);

        if (matcher.find()){
            return true;
        }
        else {
            return false;
        }
    }

    public String checkStatus(String status){
        if (status.equals("0")) {
            String status1 = "Not started yet";
            return status1;
        } else if (status.equals("1")){
            String status1 = "Working";
            return status1;
        } else if (status.equals("2")){
            String status1 = "Completed";
            return status1;
        } else {
            return "Invalid Input";
        }
    }

    public String checkImportance(String importance){
        String importance1;
        if (importance.equals("5")) {
            importance1 = "Great";
            return importance1;
        }
        else if (importance.equals("4")){
            importance1 = "Medium";
            return importance1;
        }
        else if (importance.equals("3")){
            importance1 = "Little";
            return importance1;
        }
        else {
            return "Invalid input";
        }
    }

    public boolean checkStatusNull(String status){
        if (status.isEmpty()) {
            return false;
        }
        return true;
    }

    public boolean checkImportanceNull(String importance){
        if (importance.isEmpty()) {
            return false;
        }
        return true;
    }

    public int countCategory(String category, ArrayList<GeneralWork> works){
        int c = 0;
        for (int i=0; i<works.size() ; i++){
            if (works.get(i).getType_category().equals(category)){
                c+=1;
            }
        }
        return c;
    }

    public  int countTypeCategoryInWork(ArrayList<GeneralWork> works, String category,String work_type) {
        int count = 0;
        for (int i = 0; i < works.size(); i++) {
            if ((works.get(i).getType().equals(work_type)) && (works.get(i).getType_category().equals(category))){
                    count += 1;
            }
        }
        return count;
    }



    public String swapStatus(String n){
        String o = "";
        if (n.equals("Not started yet")){
            o = "0";
        }
        else if (n.equals("Working")){
            o = "1";
        }
        else if (n.equals("Completed")){
            o = "2";
        }
        return o;
    }

    public String swapImportance(String n){
        String o = "";
        if (n.equals("Great")){
            o = "5";
        }
        else if (n.equals("Medium")){
            o = "4";
        }
        else if (n.equals("Little")){
            o = "3";
        }
        return o;
    }

}
