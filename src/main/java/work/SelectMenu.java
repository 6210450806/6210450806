package work;

import javafx.application.Application;
import javafx.stage.Stage;
import com.github.saacsos.FXRouter;

public class SelectMenu extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        FXRouter.bind(this, primaryStage, "Work Management", 900, 800);

        configRoute();

        FXRouter.goTo("Home");
    }

    private static void configRoute() {

        FXRouter.when("Home", "Home.fxml");
        FXRouter.when("Producer", "Producer.fxml");
        FXRouter.when("CreatingGeneralWork", "CreatingGeneralWork.fxml");
        FXRouter.when("MenuOfCreating", "MenuOfCreating.fxml");
        FXRouter.when("CreatingType", "CreatingType.fxml");
        FXRouter.when("CreatingForwardWork", "CreatingForwardWork.fxml");
        FXRouter.when("CreatingProjectWork", "CreatingProjectWork.fxml");
        FXRouter.when("CreatingWeekWork", "CreatingWeekWork.fxml");
        FXRouter.when("ListOfWorkAll", "ListOfWorkAll.fxml");
        FXRouter.when("MenuAll", "MenuAll.fxml");
        FXRouter.when("EditForwardWork", "EditForwardWork.fxml");
        FXRouter.when("EditGeneralWork", "EditGeneralWork.fxml");
        FXRouter.when("EditWeekWork", "EditWeekWork.fxml");
        FXRouter.when("EditProjectWork", "EditProjectWork.fxml");
        FXRouter.when("Instruction", "Instruction.fxml");

    }

    public static void main(String[] args) {
        launch(args);
    }
}
