package work.Controllers;

import com.github.saacsos.FXRouter;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import work.Controllers.Create.CreatingTypeControllers;

import java.io.IOException;

public class MenuOfCreatingControllers {


    @FXML
    private ImageView image5;

    @FXML
    public void initialize() {
        image5.setImage(new Image("/iconCute/5.png"));

    }


    @FXML
    public void CreateTypeButton(ActionEvent event) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/CreatingType.fxml"));
            Scene scene = new Scene(loader.load());

            Button b = (Button) event.getSource();
            Stage stage = (Stage) b.getScene().getWindow();

            CreatingTypeControllers controller = loader.getController();
            controller.initialize();

            stage.setTitle("Creating Category");
            stage.setScene(scene);
            stage.show();

        } catch (IOException e) {
            System.err.println("ไปที่หน้า CreatingType ไม่ได้");
            System.err.println("ให้ตรวจสอบการกำหนด route");
        }

    }


    @FXML
    public void generalWorkButton(ActionEvent actionEvent) {
        try {
            FXRouter.goTo("CreatingGeneralWork");
        } catch (IOException e) {
            System.err.println("ไปที่หน้า CreatingGeneralWork ไม่ได้");
            System.err.println("ให้ตรวจสอบการกำหนด route");
        }
    }

    @FXML
    public void weekWorkButton(ActionEvent actionEvent) {
        try {
            FXRouter.goTo("CreatingWeekWork");
        } catch (IOException e) {
            System.err.println("ไปที่หน้า CreatingWeekWork ไม่ได้");
            System.err.println("ให้ตรวจสอบการกำหนด route");
        }
    }

    @FXML
    public void projectWorkButton(ActionEvent actionEvent) {
        try {
            FXRouter.goTo("CreatingProjectWork");
        } catch (IOException e) {
            System.err.println("ไปที่หน้า CreatingProject ไม่ได้");
            System.err.println("ให้ตรวจสอบการกำหนด route");
        }
    }

    @FXML
    public void forwardWorkButton(ActionEvent actionEvent) {
        try {
            FXRouter.goTo("CreatingForwardWork");
        } catch (IOException e) {
            System.err.println("ไปที่หน้า CreatingForwardWork ไม่ได้");
            System.err.println("ให้ตรวจสอบการกำหนด route");
        }
    }

    @FXML
    public void menuAllButton(ActionEvent actionEvent) {
        try {
            FXRouter.goTo("MenuAll");
        } catch (IOException e) {
            System.err.println("ไปที่หน้า MenuAll ไม่ได้");
            System.err.println("ให้ตรวจสอบการกำหนด route");
        }
    }


}
