package work.Controllers;

import com.github.saacsos.FXRouter;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

import java.io.IOException;

public class MenuAllControllers {

    @FXML private ImageView image7;
    @FXML private ImageView image8;

    @FXML
    public void initialize() {
        image7.setImage(new Image("/iconCute/7.png"));
        image8.setImage(new Image("/iconCute/8.png"));
    }



    @FXML
    public void menuButton(ActionEvent actionEvent) {
        try {
            FXRouter.goTo("MenuOfCreating");
        } catch (IOException e) {
            System.err.println("ไปที่หน้า MenuOfCreating ไม่ได้");
            System.err.println("ให้ตรวจสอบการกำหนด route");
        }
    }


    @FXML
    public void listButton(ActionEvent event) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/ListOfWorkAll.fxml"));
            Scene scene = new Scene(loader.load());

            Button b = (Button) event.getSource();
            Stage stage = (Stage) b.getScene().getWindow();

            ListOfWorkAllControllers controller = loader.getController();
            controller.initializeText();
            controller.initializeList();

            stage.setTitle("Work Management");
            stage.setScene(scene);
            stage.show();
        }
        catch (IOException e) {
            System.err.println("ไปที่หน้า ListOfWorkAll ไม่ได้");
            System.err.println("ให้ตรวจสอบการกำหนด route");
        }
    }

    @FXML
    public void backHomeButton(ActionEvent actionEvent) {
        try {
            FXRouter.goTo("Home");
        } catch (IOException e) {
            System.err.println("ไปที่หน้า Home ไม่ได้");
            System.err.println("ให้ตรวจสอบการกำหนด route");
        }
    }

}
