package work.Controllers.Edit;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import work.AllUse.EverythingCheck;
import work.AllUse.RWfile;
import work.Models.GeneralWork;
import work.Models.ProjectWork;
import work.Models.Type;
import work.Controllers.ListOfWorkAllControllers;
import work.Controllers.Create.CreatingTypeControllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;

public class EditProjectWorkControllers {
    @FXML TextField p_nameTextField;
    @FXML DatePicker p_dayStartTextField;
    @FXML DatePicker p_dayEndTextField;
    @FXML TextField p_statusTextField;
    @FXML TextField p_impTextField;
    @FXML TextField p_nameLeaderTextField;
    @FXML ComboBox select_category_combobox;

    private ProjectWork work_tmp;
    private int index;
    RWfile rw = new RWfile();
    EverythingCheck ch = new EverythingCheck();
    Alert alert;

    @FXML public void initializeText() throws IOException {
        ArrayList<Type> types = rw.readTypeDataBase();
        for (int i=0;i<types.size();i++){
            select_category_combobox.getItems().addAll(types.get(i).getNameType());
        }

        p_nameTextField.setText(work_tmp.getName_work());
        p_dayStartTextField.getEditor().setText(work_tmp.getDay_start());
        p_dayEndTextField.getEditor().setText(work_tmp.getDay_end());
        p_nameLeaderTextField.setText(work_tmp.getName_leader());

        Object category = work_tmp.getType_category();
        select_category_combobox.setValue(category);

        String status = ch.swapStatus(work_tmp.getStatus());
        String imp = ch.swapImportance(work_tmp.getImportance());

        p_statusTextField.setText(status);
        p_impTextField.setText(imp);
    }

    @FXML public void handleAddConfirmButton(ActionEvent event) throws IOException {
        String name = p_nameTextField.getText();
        String day_start = p_dayStartTextField.getEditor().getText();
        String imp = p_impTextField.getText();
        String status = p_statusTextField.getText();
        String day_end = p_dayEndTextField.getEditor().getText();
        String name_leader = p_nameLeaderTextField.getText();
        Object category = select_category_combobox.getValue();
        String category_string = String.valueOf(category);
        ArrayList<GeneralWork> works = rw.readWorkDataBase();
        int flag = 1;
        int flag_check_name = 0;
        int flag_imp = 1;

        if (this.p_nameTextField.getText().isEmpty()) {
            alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(" ");
            alert.setContentText("Please enter name work");
            alert.showAndWait();
            flag = 0;
            flag_check_name = 1;
        }

        else if (this.p_nameLeaderTextField.getText().isEmpty()) {

            alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(" ");
            alert.setContentText("Please enter name leader of work");
            alert.showAndWait();

        }
        else if (this.p_dayStartTextField.getEditor().getText().isEmpty()) {

            alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(" ");
            alert.setContentText("Please enter day start at work");
            alert.showAndWait();

        }
        else if (this.p_dayEndTextField.getEditor().getText().isEmpty()) {

            alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(" ");
            alert.setContentText("Please enter day end at work");
            alert.showAndWait();

        }
        else if (ch.checkStatusNull(p_statusTextField.getText())) {
            status = ch.checkStatus(p_statusTextField.getText());
            if (status.equals("Invalid Input")){
                alert = new Alert(Alert.AlertType.ERROR);
                alert.setHeaderText(" ");
                alert.setContentText("Please read attention of status");
                alert.showAndWait();
            }
            else {
                flag = 0;
            }
        }
        else if  (!ch.checkStatusNull(p_statusTextField.getText())) {
            alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(" ");
            alert.setContentText("Please enter status of work");
            alert.showAndWait();
        }

        if ((ch.checkImportanceNull(p_impTextField.getText())) && (flag !=1 )) {
            imp = ch.checkImportance(p_impTextField.getText());
            if (imp.equals("Invalid input")){
                alert = new Alert(Alert.AlertType.ERROR);
                alert.setHeaderText(" ");
                alert.setContentText("Please read attention of importance");
                alert.showAndWait();
                System.out.println(imp);
                flag_imp = 0;
            }

        }
        else if  (!ch.checkImportanceNull(p_impTextField.getText())){
            alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(" ");
            alert.setContentText("Please enter importance of work");
            alert.showAndWait();
            flag_imp = 0;
        }

        if (category_string.equals("null")) {
            alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(" ");
            alert.setContentText("Please select category of work");
            alert.showAndWait();

        }
        else if (this.select_category_combobox.equals("null")) {

            alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(" ");
            alert.setContentText("Please select category of work");
            alert.showAndWait();

        }

        else {
            int check = 0;
            if (flag_check_name==0){
                if (!ch.checkNameType(p_nameTextField.getText())){
                    alert = new Alert(Alert.AlertType.ERROR);
                    alert.setHeaderText(" ");
                    alert.setContentText("Please check your name work information");
                    alert.showAndWait();
                    check = 1;
                }

            }
            if (flag_check_name==0){
                for (int i=0 ; i<works.size() ; i++){
                    if ((works.get(i).getName_work().equals(p_nameTextField.getText())) && (i!=index)){
                        alert = new Alert(Alert.AlertType.ERROR);
                        alert.setHeaderText(" ");
                        alert.setContentText("Duplicate name !");
                        alert.showAndWait();
                        check = 1;
                        break;
                    }
                }
            }

            if (check == 0 && flag_check_name == 0 && flag_imp == 1) {
                String type = "project";
                status = ch.checkStatus(p_statusTextField.getText());
                imp = ch.checkImportance(p_impTextField.getText());
                GeneralWork projectWork = new ProjectWork(imp, status, type, category_string, name, name_leader, day_start, day_end, type);
                projectWork.setType(type);
                projectWork.setName_work(name);
                System.out.println(projectWork.toString());

                Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Edit Work Of Project ?",
                        ButtonType.OK,
                        ButtonType.CANCEL);
                alert.setTitle("Edit Work of project.");
                Optional<ButtonType> result = alert.showAndWait();

                if (result.get() == ButtonType.OK) {
                    works.remove(index);
                    works.add(index,projectWork);
                    rw.writeWorkDataBase(works);

                    try {
                        FXMLLoader loader = new FXMLLoader();
                        loader.setLocation(getClass().getResource("/ListOfWorkAll.fxml"));
                        Scene scene = new Scene(loader.load());

                        Button b = (Button) event.getSource();
                        Stage stage = (Stage) b.getScene().getWindow();

                        ListOfWorkAllControllers controller = loader.getController();
                        controller.initializeText();
                        controller.initializeList();

                        stage.setTitle("List of work");
                        stage.setScene(scene);
                        stage.show();
                    }
                    catch (IOException e) {
                        System.err.println("ไปที่หน้า ListOfWorkAll ไม่ได้");
                        System.err.println("ให้ตรวจสอบการกำหนด route");
                    }
                }

            }

        }
    }

    @FXML
    public void listButton(ActionEvent event) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/ListOfWorkAll.fxml"));
            Scene scene = new Scene(loader.load());

            Button b = (Button) event.getSource();
            Stage stage = (Stage) b.getScene().getWindow();

            ListOfWorkAllControllers controller = loader.getController();
            controller.initializeText();
            controller.initializeList();

            stage.setTitle("work list");
            stage.setScene(scene);
            stage.show();
        }
        catch (IOException e) {
            System.err.println("ไปที่หน้า ListOfWorkAll ไม่ได้");
            System.err.println("ให้ตรวจสอบการกำหนด route");
        }
    }

    @FXML public void CreateTypeButton(ActionEvent event) {
        try {
            FXMLLoader loader = new FXMLLoader(); // create loader
            loader.setLocation(getClass().getResource("/CreatingType.fxml"));
            Scene scene = new Scene(loader.load());

            Button b = (Button) event.getSource();
            Stage stage = (Stage) b.getScene().getWindow();

            CreatingTypeControllers controller = loader.getController();
            controller.initialize();

            stage.setTitle("Creating Category");
            stage.setScene(scene);
            stage.show();

        } catch (IOException e) {
            System.err.println("ไปที่หน้า CreatingType ไม่ได้");
            System.err.println("ให้ตรวจสอบการกำหนด route");
        }
    }

    public void setWorkTmp(ProjectWork work_tmp) {
        this.work_tmp = work_tmp;
    }
    public void setIndex(int index){
        this.index = index;
    }
}
