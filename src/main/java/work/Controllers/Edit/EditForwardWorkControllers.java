package work.Controllers.Edit;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import work.AllUse.EverythingCheck;
import work.AllUse.RWfile;
import work.Models.ForwardWork;
import work.Models.GeneralWork;
import work.Models.Type;
import work.Controllers.ListOfWorkAllControllers;
import work.Controllers.Create.CreatingTypeControllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;

public class EditForwardWorkControllers {

    @FXML TextField f_nameTextField;
    @FXML DatePicker f_dayDatePicker;
    @FXML TextField f_startTextField;
    @FXML TextField f_statusTextField;
    @FXML TextField f_impTextField;
    @FXML TextField f_namePersonTextField;
    @FXML ComboBox select_category_combobox;
    private ForwardWork work_tmp;
    private int index;
    RWfile rw = new RWfile();
    EverythingCheck ch = new EverythingCheck();
    Alert alert;

    @FXML
    public void initializeText() throws IOException {
        ArrayList<Type> types = this.rw.readTypeDataBase();

        for(int i = 0; i < types.size(); ++i) {
            select_category_combobox.getItems().addAll(new Object[]{((Type)types.get(i)).getNameType()});
        }

        f_nameTextField.setText(work_tmp.getName_work());
        f_dayDatePicker.getEditor().setText(work_tmp.getDay());
        f_startTextField.setText(work_tmp.getStart());
        f_namePersonTextField.setText(work_tmp.getName_res());
        Object category = work_tmp.getType_category();
        select_category_combobox.setValue(category);
        String status = ch.swapStatus(work_tmp.getStatus());
        String imp = ch.swapImportance(work_tmp.getImportance());
        f_statusTextField.setText(status);
        f_impTextField.setText(imp);
    }

    @FXML
    public void handleAddConfirmButton(ActionEvent event) throws IOException {
        String name = f_nameTextField.getText();
        String day = f_dayDatePicker.getEditor().getText();
        String imp ;
        String status = f_statusTextField.getText();
        String start = f_startTextField.getText();
        String name_p = f_namePersonTextField.getText();
        Object category = select_category_combobox.getValue();
        String category_string = String.valueOf(category);
        ArrayList<GeneralWork> works = rw.readWorkDataBase();
        int flag = 1;
        int flag_imp = 1;
        int flag_check_name = 0;
        if (f_nameTextField.getText().isEmpty()) {
            alert = new Alert(AlertType.ERROR);
            alert.setHeaderText(" ");
            alert.setContentText("Please enter name work");
            alert.showAndWait();
            flag = 0;
            flag_check_name = 1;
        } else if (f_dayDatePicker.getEditor().getText().isEmpty()) {
            alert = new Alert(AlertType.ERROR);
            alert.setHeaderText(" ");
            alert.setContentText("Please enter day at work");
            alert.showAndWait();
        } else if (f_namePersonTextField.getText().isEmpty()) {
            alert = new Alert(AlertType.ERROR);
            alert.setHeaderText(" ");
            alert.setContentText("Please enter name of Responsible person");
            alert.showAndWait();
            System.out.println("ok");
        } else if (f_startTextField.getText().isEmpty()) {
            alert = new Alert(AlertType.ERROR);
            alert.setHeaderText(" ");
            alert.setContentText("Please enter time to start at work");
            alert.showAndWait();
            System.out.println("ok");
        } else if (!ch.checkTime(f_startTextField.getText())) {
            alert = new Alert(AlertType.ERROR);
            alert.setHeaderText(" ");
            alert.setContentText("Please check your start time information !");
            alert.showAndWait();
        } else if (ch.checkStatusNull(f_statusTextField.getText())) {
            status = ch.checkStatus(f_statusTextField.getText());
            if (status.equals("Invalid Input")) {
                alert = new Alert(AlertType.ERROR);
                alert.setHeaderText(" ");
                alert.setContentText("Please read attention of status");
                alert.showAndWait();
            } else {
                flag = 0;
            }
        } else if (!ch.checkStatusNull(f_statusTextField.getText())) {
            alert = new Alert(AlertType.ERROR);
            alert.setHeaderText(" ");
            alert.setContentText("Please enter status of work");
            alert.showAndWait();
        }

        if ((ch.checkImportanceNull(f_impTextField.getText()) && (flag != 1))) {
            imp = ch.checkImportance(f_impTextField.getText());
            if (imp.equals("Invalid input")) {
                alert = new Alert(AlertType.ERROR);
                alert.setHeaderText(" ");
                alert.setContentText("Please read attention of importance");
                alert.showAndWait();
                flag_imp = 0;

            }
        }

        if (!ch.checkImportanceNull(f_impTextField.getText())) {
            alert = new Alert(AlertType.ERROR);
            alert.setHeaderText(" ");
            alert.setContentText("Please enter importance of work");
            alert.showAndWait();
            flag_imp = 0;
        }
        if (category_string.equals("null")) {
            alert = new Alert(AlertType.ERROR);
            alert.setHeaderText(" ");
            alert.setContentText("Please select category of work");
            alert.showAndWait();
        }
        else {
            int check = 0;
            if (flag_check_name == 0 && !ch.checkNameType(f_nameTextField.getText())) {
                this.alert = new Alert(AlertType.ERROR);
                this.alert.setHeaderText(" ");
                this.alert.setContentText("Please check your name work information");
                this.alert.showAndWait();
                check = 1;
            }

            if (flag_check_name == 0) {
                for(int i = 0; i < works.size(); ++i) {
                    if ((works.get(i)).getName_work().equals(f_nameTextField.getText()) && i != index) {
                        alert = new Alert(AlertType.ERROR);
                        alert.setHeaderText(" ");
                        alert.setContentText("Duplicate name !");
                        alert.showAndWait();
                        check = 1;
                        break;
                    }
                }
            }

            System.out.println(check);
            System.out.println(flag_check_name);

            if (check == 0 && flag_check_name == 0 && flag_imp == 1) {
                String type = "forward";
                status = ch.checkStatus(f_statusTextField.getText());
                imp = ch.checkImportance(f_impTextField.getText());
                GeneralWork forward_work_tmp = new ForwardWork(day, imp, status, start, name, name_p, category_string, type);
                System.out.println(forward_work_tmp.toString());
                Alert alert = new Alert(AlertType.CONFIRMATION, "Edit Work Of Forward ?", new ButtonType[]{ButtonType.OK, ButtonType.CANCEL});
                alert.setTitle("Edit Work of forward.");
                Optional<ButtonType> result = alert.showAndWait();
                if (result.get() == ButtonType.OK) {
                    works.remove(index);
                    works.add(index, forward_work_tmp);
                    rw.writeWorkDataBase(works);

                    try {
                        FXMLLoader loader = new FXMLLoader();
                        loader.setLocation(getClass().getResource("/ListOfWorkAll.fxml"));
                        Scene scene = new Scene(loader.load());
                        Button b = (Button)event.getSource();
                        Stage stage = (Stage)b.getScene().getWindow();
                        ListOfWorkAllControllers controller = loader.getController();
                        controller.initializeText();
                        controller.initializeList();
                        stage.setTitle("List of work");
                        stage.setScene(scene);
                        stage.show();
                    } catch (IOException var23) {
                        System.err.println("ไปที่หน้า ListOfWorkAll ไม่ได้");
                        System.err.println("ให้ตรวจสอบการกำหนด route");
                    }
                }
            }
        }

    }

    @FXML
    public void listButton(ActionEvent event) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(this.getClass().getResource("/ListOfWorkAll.fxml"));
            Scene scene = new Scene(loader.load());
            Button b = (Button)event.getSource();
            Stage stage = (Stage)b.getScene().getWindow();
            ListOfWorkAllControllers controller = loader.getController();
            controller.initializeText();
            controller.initializeList();
            stage.setTitle("work list");
            stage.setScene(scene);
            stage.show();
        } catch (IOException var7) {
            System.err.println("ไปที่หน้า ListOfWorkAll ไม่ได้");
            System.err.println("ให้ตรวจสอบการกำหนด route");
        }

    }

    @FXML
    public void CreateTypeButton(ActionEvent event) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(this.getClass().getResource("/CreatingType.fxml"));
            Scene scene = new Scene(loader.load());
            Button b = (Button)event.getSource();
            Stage stage = (Stage)b.getScene().getWindow();
            CreatingTypeControllers controller = loader.getController();
            controller.initialize();
            stage.setTitle("Creating Category");
            stage.setScene(scene);
            stage.show();
        } catch (IOException var7) {
            System.err.println("ไปที่หน้า CreatingType ไม่ได้");
            System.err.println("ให้ตรวจสอบการกำหนด route");
        }

    }

    public void setWorkTmp(ForwardWork work_tmp) {
        this.work_tmp = work_tmp;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
