package work.Controllers.Edit;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import work.AllUse.EverythingCheck;
import work.AllUse.RWfile;
import work.Models.GeneralWork;
import work.Models.Type;
import work.Models.WeekWork;
import work.Controllers.ListOfWorkAllControllers;
import work.Controllers.Create.CreatingTypeControllers;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;

public class EditWeekWorkControllers {
    @FXML TextField w_nameTextField;
    @FXML TextField w_dayDatePicker;
    @FXML TextField w_startTextField;
    @FXML TextField w_endTextField;
    @FXML TextField w_statusTextField;
    @FXML TextField w_impTextField;
    @FXML ComboBox select_category_combobox;
    private WeekWork work_tmp;
    private int index;
    RWfile rw = new RWfile();
    EverythingCheck ch = new EverythingCheck();
    Alert alert;

    @FXML public void initializeText() throws IOException {
        ArrayList<Type> types = rw.readTypeDataBase();
        for (int i=0;i<types.size();i++){
            select_category_combobox.getItems().addAll(types.get(i).getNameType());
        }

        w_nameTextField.setText(work_tmp.getName_work());
        w_dayDatePicker.setText(work_tmp.getWday());
        w_startTextField.setText(work_tmp.getWstart());
        w_endTextField.setText(work_tmp.getWend());

        Object category = work_tmp.getType_category();
        select_category_combobox.setValue(category);

        String status = ch.swapStatus(work_tmp.getStatus());
        String imp = ch.swapImportance(work_tmp.getImportance());

        w_statusTextField.setText(status);
        w_impTextField.setText(imp);
    }

    @FXML public void handleAddConfirmButton(ActionEvent event) throws IOException {
        String name = w_nameTextField.getText();
        String day = w_dayDatePicker.getText();
        String imp = w_impTextField.getText();
        String status = w_statusTextField.getText();
        String start = w_startTextField.getText();
        String end = w_endTextField.getText();
        Object category = select_category_combobox.getValue();
        String category_string = String.valueOf(category);
        ArrayList<GeneralWork> works = rw.readWorkDataBase();
        int flag = 1;
        int flag_check_name = 0;
        int flag_imp = 1;

        if (this.w_nameTextField.getText().isEmpty()) {
            alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(" ");
            alert.setContentText("Please enter name work");
            alert.showAndWait();
            flag = 0;
            flag_check_name = 1;
        }

        else if (this.w_dayDatePicker.getText().isEmpty()) {

            alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(" ");
            alert.setContentText("Please enter day at work");
            alert.showAndWait();

        }
        else if (!ch.checkTime(w_startTextField.getText())) {
            alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(" ");
            alert.setContentText("Please check your start time information !");
            alert.showAndWait();
            System.out.println("ok");
        }
        else if (!ch.checkTime(w_endTextField.getText())) {
            alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(" ");
            alert.setContentText("Please check your end time information !");
            alert.showAndWait();
        }

        else if (ch.checkStatusNull(w_statusTextField.getText())) {
            status = ch.checkStatus(w_statusTextField.getText());
            if (status.equals("Invalid Input")){
                alert = new Alert(Alert.AlertType.ERROR);
                alert.setHeaderText(" ");
                alert.setContentText("Please read attention of status");
                alert.showAndWait();
            }
            else {
                flag = 0;
            }
        }
        else if  (!ch.checkStatusNull(w_statusTextField.getText())) {
            alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(" ");
            alert.setContentText("Please enter status of work");
            alert.showAndWait();
        }
        if ((ch.checkImportanceNull(w_impTextField.getText())) && (flag != 1)) {
            imp = ch.checkImportance(w_impTextField.getText());
            if (imp.equals("Invalid input")){
                alert = new Alert(Alert.AlertType.ERROR);
                alert.setHeaderText(" ");
                alert.setContentText("Please read attention of importance");
                alert.showAndWait();
                System.out.println(imp);
                flag_imp = 0;
            }

        }
        else if  (!ch.checkImportanceNull(w_impTextField.getText())){
            alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(" ");
            alert.setContentText("Please enter importance of work");
            alert.showAndWait();
            flag_imp = 0;
        }
        if (category_string.equals("null")) {
            alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(" ");
            alert.setContentText("Please select category of work");
            alert.showAndWait();

        }

        else {
            int check = 0;
            if (flag_check_name==0){
                if (!ch.checkNameType(w_nameTextField.getText())){
                    alert = new Alert(Alert.AlertType.ERROR);
                    alert.setHeaderText(" ");
                    alert.setContentText("Please check your name work information");
                    alert.showAndWait();
                    check = 1;
                }

            }
            if (flag_check_name==0){
                for (int i=0 ; i<works.size() ; i++){
                    if ((works.get(i).getName_work().equals(w_nameTextField.getText())) && (i!=index)){
                        alert = new Alert(Alert.AlertType.ERROR);
                        alert.setHeaderText(" ");
                        alert.setContentText("Duplicate name !");
                        alert.showAndWait();
                        check = 1;
                        break;
                    }
                }
            }

            if (check == 0 && flag_check_name == 0 && flag_imp == 1) {
                String type = "week";
                status = ch.checkStatus(w_statusTextField.getText());
                imp = ch.checkImportance(w_impTextField.getText());
                GeneralWork weekWork = new WeekWork(imp, status, name, day, start, end, type, category_string);
                weekWork.setType("week");
                weekWork.setName_work(name);
                System.out.println(weekWork.toString());

                Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Edit Work Of Week ?",
                        ButtonType.OK,
                        ButtonType.CANCEL);
                alert.setTitle("Edit Work of week.");
                Optional<ButtonType> result = alert.showAndWait();

                if (result.get() == ButtonType.OK) {
                    works.remove(index);
                    works.add(index,weekWork);
                    rw.writeWorkDataBase(works);

                    try {
                        FXMLLoader loader = new FXMLLoader();
                        loader.setLocation(getClass().getResource("/ListOfWorkAll.fxml"));
                        Scene scene = new Scene(loader.load());

                        Button b = (Button) event.getSource();
                        Stage stage = (Stage) b.getScene().getWindow();

                        ListOfWorkAllControllers controller = loader.getController();
                        controller.initializeText();
                        controller.initializeList();

                        stage.setTitle("List of work");
                        stage.setScene(scene);
                        stage.show();
                    }
                    catch (IOException e) {
                        System.err.println("ไปที่หน้า ListOfWorkAll ไม่ได้");
                        System.err.println("ให้ตรวจสอบการกำหนด route");
                    }
                }
            }

        }
    }


    @FXML public void listButton(ActionEvent event) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/ListOfWorkAll.fxml"));
            Scene scene = new Scene(loader.load());

            Button b = (Button) event.getSource();
            Stage stage = (Stage) b.getScene().getWindow();

            ListOfWorkAllControllers controller = loader.getController();
            controller.initializeText();
            controller.initializeList();

            stage.setTitle("work list");
            stage.setScene(scene);
            stage.show();
        }
        catch (IOException e) {
            System.err.println("ไปที่หน้า ListOfWorkAll ไม่ได้");
            System.err.println("ให้ตรวจสอบการกำหนด route");
        }
    }

    @FXML public void CreateTypeButton(ActionEvent event) {
        try {
            FXMLLoader loader = new FXMLLoader(); // create loader
            loader.setLocation(getClass().getResource("/CreatingType.fxml"));
            Scene scene = new Scene(loader.load());

            Button b = (Button) event.getSource();
            Stage stage = (Stage) b.getScene().getWindow();

            CreatingTypeControllers controller = loader.getController();
            controller.initialize();

            stage.setTitle("Creating Category");
            stage.setScene(scene);
            stage.show();

        } catch (IOException e) {
            System.err.println("ไปที่หน้า CreatingType ไม่ได้");
            System.err.println("ให้ตรวจสอบการกำหนด route");
        }
    }

    public void setWorkTmp(WeekWork weekWork) {
        this.work_tmp = weekWork;
    }
    public void setIndex(int index){
        this.index = index;
    }

}
