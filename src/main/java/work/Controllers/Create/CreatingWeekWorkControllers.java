package work.Controllers.Create;

import com.github.saacsos.FXRouter;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import work.AllUse.EverythingCheck;
import work.AllUse.RWfile;
import work.Models.GeneralWork;
import work.Models.Type;
import work.Models.WeekWork;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;

public class CreatingWeekWorkControllers {

    @FXML TextField w_nameTextField;
    @FXML TextField w_dayDatePicker;
    @FXML TextField w_startTextField;
    @FXML TextField w_endTextField;
    @FXML TextField w_statusTextField;
    @FXML TextField w_impTextField;
    @FXML ComboBox select_category_combobox;
    private WeekWork weekWork;
    RWfile rw = new RWfile();
    EverythingCheck ch = new EverythingCheck();
    Alert alert;
    private int index;


    @FXML
    public void initialize() throws IOException {
        ArrayList<Type> types = rw.readTypeDataBase();

        for(int i = 0; i < types.size(); ++i) {
            select_category_combobox.getItems().addAll(new Object[]{(types.get(i)).getNameType()});
        }

        System.out.println("initialize WeekWorkController");
        weekWork = (WeekWork)FXRouter.getData();
    }

    @FXML
    public void handleAddConfirmButton(ActionEvent actionEvent) throws IOException {
        String name = w_nameTextField.getText();
        String day = w_dayDatePicker.getText();
        String imp = w_impTextField.getText();
        String status = w_statusTextField.getText();
        String start = w_startTextField.getText();
        String end = w_endTextField.getText();
        Object category = select_category_combobox.getValue();
        String category_string = String.valueOf(category);
        ArrayList<GeneralWork> works = this.rw.readWorkDataBase();
        int flag = 1;
        int flag_check_name = 0;
        int flag_imp = 1;
        if (this.w_nameTextField.getText().isEmpty()) {
            alert = new Alert(AlertType.ERROR);
            alert.setHeaderText(" ");
            alert.setContentText("Please enter name work");
            alert.showAndWait();
            flag = 0;
        } else if (this.w_dayDatePicker.getText().isEmpty()) {
            alert = new Alert(AlertType.ERROR);
            alert.setHeaderText(" ");
            alert.setContentText("Please enter day at work");
            alert.showAndWait();
        } else if (!ch.checkTime(this.w_startTextField.getText())) {
            alert = new Alert(AlertType.ERROR);
            alert.setHeaderText(" ");
            alert.setContentText("Please check your start time information !");
            alert.showAndWait();
            System.out.println("ok");
        } else if (!ch.checkTime(this.w_endTextField.getText())) {
            alert = new Alert(AlertType.ERROR);
            alert.setHeaderText(" ");
            alert.setContentText("Please check your end time information !");
            alert.showAndWait();}
        else if (ch.checkStatusNull(this.w_statusTextField.getText())) {
            status = ch.checkStatus(this.w_statusTextField.getText());
            if (status.equals("Invalid Input")) {
                alert = new Alert(AlertType.ERROR);
                alert.setHeaderText(" ");
                alert.setContentText("Please read attention of status");
                alert.showAndWait();
            } else {
                flag = 0;
            }
        } else if (!ch.checkStatusNull(this.w_statusTextField.getText())) {
            alert = new Alert(AlertType.ERROR);
            alert.setHeaderText(" ");
            alert.setContentText("Please enter status of work");
            alert.showAndWait();
        }

        if (ch.checkImportanceNull(this.w_impTextField.getText()) && flag != 1) {
            imp = ch.checkImportance(this.w_impTextField.getText());
            if (imp.equals("Invalid input")) {
                alert = new Alert(AlertType.ERROR);
                alert.setHeaderText(" ");
                alert.setContentText("Please read attention of importance");
                alert.showAndWait();
                System.out.println(imp);
                flag_imp = 0;
            }
        } else if (!ch.checkImportanceNull(this.w_impTextField.getText())) {
            alert = new Alert(AlertType.ERROR);
            alert.setHeaderText(" ");
            alert.setContentText("Please enter importance of work");
            alert.showAndWait();
            flag_imp = 0 ;
        }
        if (category_string.equals("null")) {
            alert = new Alert(AlertType.ERROR);
            alert.setHeaderText(" ");
            alert.setContentText("Please select category of work");
            alert.showAndWait();
        } else {
            int check = 0;
            if (flag_check_name == 0 && !ch.checkNameType(this.w_nameTextField.getText())) {
                alert = new Alert(AlertType.ERROR);
                alert.setHeaderText(" ");
                alert.setContentText("Please check your name work information");
                alert.showAndWait();
                check = 1;
            }

            if (flag_check_name == 0) {
                for(int i = 0; i < works.size(); ++i) {
                    if ((works.get(i)).getName_work().equals(this.w_nameTextField.getText()) && i != index) {
                        alert = new Alert(AlertType.ERROR);
                        alert.setHeaderText(" ");
                        alert.setContentText("Duplicate name !");
                        alert.showAndWait();
                        check = 1;
                        break;
                    }
                }
            }

            if (check == 0 && flag_check_name == 0 && flag_imp == 1) {
                String type = "week";
                imp = ch.checkImportance(this.w_impTextField.getText());
                GeneralWork weekWork = new WeekWork(imp, status, name, day, start, end, type, category_string);
                weekWork.setType("week");
                weekWork.setName_work(name);
                System.out.println(weekWork.toString());
                Alert alert = new Alert(AlertType.CONFIRMATION, "Create new Work Of Week ?", new ButtonType[]{ButtonType.OK, ButtonType.CANCEL});
                alert.setTitle("Create new Work of week.");
                Optional<ButtonType> result = alert.showAndWait();
                if (result.get() == ButtonType.OK) {
                    works.add(weekWork);
                    rw.writeWorkDataBase(works);
                    w_nameTextField.clear();
                    w_dayDatePicker.clear();
                    w_startTextField.clear();
                    w_endTextField.clear();
                    w_statusTextField.clear();
                    w_impTextField.clear();
                    select_category_combobox.getItems().clear();
                    initialize();
                }
            }
        }

    }

    @FXML
    public void handleBackButton(ActionEvent actionEvent) {
        try {
            FXRouter.goTo("MenuOfCreating");
        } catch (IOException var3) {
            System.err.println("ไปที่หน้า MenuOfCreating ไม่ได้");
            System.err.println("ให้ตรวจสอบการกำหนด route");
        }

    }

    @FXML
    public void CreateTypeButton(ActionEvent event) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/CreatingType.fxml"));
            Scene scene = new Scene(loader.load());
            Button b = (Button)event.getSource();
            Stage stage = (Stage)b.getScene().getWindow();
            CreatingTypeControllers controller = loader.getController();
            controller.initialize();
            stage.setTitle("Creating Category");
            stage.setScene(scene);
            stage.show();
        } catch (IOException var7) {
            System.err.println("ไปที่หน้า CreatingType ไม่ได้");
            System.err.println("ให้ตรวจสอบการกำหนด route");
        }

    }
}
