package work.Controllers.Create;

import com.github.saacsos.FXRouter;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import work.AllUse.EverythingCheck;
import work.AllUse.RWfile;
import work.Models.GeneralWork;
import work.Models.ProjectWork;
import work.Models.Type;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;

public class CreatingProjectWorkControllers {

    @FXML TextField p_nameTextField;
    @FXML DatePicker p_dayStartTextField;
    @FXML DatePicker p_dayEndTextField;
    @FXML TextField p_statusTextField;
    @FXML TextField p_impTextField;
    @FXML TextField p_nameLeaderTextField;
    @FXML ComboBox select_category_combobox;
    private ProjectWork projectWork;
    RWfile rw = new RWfile();
    EverythingCheck ch = new EverythingCheck();
    Alert alert;
    private int index;


    @FXML
    public void initialize() throws IOException {
        ArrayList<Type> types = rw.readTypeDataBase();

        for(int i = 0; i < types.size(); ++i) {
            select_category_combobox.getItems().addAll(new Object[]{(types.get(i)).getNameType()});
        }

        System.out.println("initialize ProjectWorkController");
        projectWork = (ProjectWork)FXRouter.getData();
    }

    @FXML
    public void handleAddConfirmButton(ActionEvent actionEvent) throws IOException {
        String name = p_nameTextField.getText();
        String day_start = p_dayStartTextField.getEditor().getText();
        String imp = this.p_impTextField.getText();
        String status = p_statusTextField.getText();
        String day_end = p_dayEndTextField.getEditor().getText();
        String name_leader = p_nameLeaderTextField.getText();
        Object category = select_category_combobox.getValue();
        String category_string = String.valueOf(category);
        ArrayList<GeneralWork> works = rw.readWorkDataBase();
        int flag = 1;
        int flag_check_name = 0;
        int flag_imp = 1;
        if (this.p_nameTextField.getText().isEmpty()) {
            alert = new Alert(AlertType.ERROR);
            alert.setHeaderText(" ");
            alert.setContentText("Please enter name work");
            alert.showAndWait();
        } else if (this.p_nameLeaderTextField.getText().isEmpty()) {
            alert = new Alert(AlertType.ERROR);
            alert.setHeaderText(" ");
            alert.setContentText("Please enter name leader of work");
            alert.showAndWait();
        } else if (this.p_dayStartTextField.getEditor().getText().isEmpty()) {
            alert = new Alert(AlertType.ERROR);
            alert.setHeaderText(" ");
            alert.setContentText("Please enter day start at work");
            alert.showAndWait();
        } else if (this.p_dayEndTextField.getEditor().getText().isEmpty()) {
            alert = new Alert(AlertType.ERROR);
            alert.setHeaderText(" ");
            alert.setContentText("Please enter day end at work");
            alert.showAndWait();
        } else if (this.ch.checkStatusNull(this.p_statusTextField.getText())) {
            status = ch.checkStatus(this.p_statusTextField.getText());
            if (status.equals("Invalid Input")) {
                alert = new Alert(AlertType.ERROR);
                alert.setHeaderText(" ");
                alert.setContentText("Please read attention of status");
                alert.showAndWait();
            } else {
                flag = 0;
            }
        } else if (!ch.checkStatusNull(this.p_statusTextField.getText())) {
            alert = new Alert(AlertType.ERROR);
            alert.setHeaderText(" ");
            alert.setContentText("Please enter status of work");
            alert.showAndWait();
        }

        if (ch.checkImportanceNull(this.p_impTextField.getText()) && flag != 1) {
            imp = ch.checkImportance(this.p_impTextField.getText());
            if (imp.equals("Invalid input")) {
                alert = new Alert(AlertType.ERROR);
                alert.setHeaderText(" ");
                alert.setContentText("Please read attention of importance");
                alert.showAndWait();
                System.out.println(imp);
                flag_imp = 0;
            }
        } else if (!ch.checkImportanceNull(this.p_impTextField.getText())) {
            alert = new Alert(AlertType.ERROR);
            alert.setHeaderText(" ");
            alert.setContentText("Please enter importance of work");
            alert.showAndWait();
            flag_imp = 0;
        }
        if (category_string.equals("null")) {
            alert = new Alert(AlertType.ERROR);
            alert.setHeaderText(" ");
            alert.setContentText("Please select category of work");
            alert.showAndWait();
        } else if (select_category_combobox.equals("null")) {
            alert = new Alert(AlertType.ERROR);
            alert.setHeaderText(" ");
            alert.setContentText("Please select category of work");
            alert.showAndWait();
        } else {
            int check = 0;
            if (flag_check_name == 0 && !ch.checkNameType(this.p_nameTextField.getText())) {
                alert = new Alert(AlertType.ERROR);
                alert.setHeaderText(" ");
                alert.setContentText("Please check your name work information");
                alert.showAndWait();
                check = 1;
            }

            if (flag_check_name == 0) {
                for(int i = 0; i < works.size(); ++i) {
                    if ((works.get(i)).getName_work().equals(this.p_nameTextField.getText()) && i != index) {
                        alert = new Alert(AlertType.ERROR);
                        alert.setHeaderText(" ");
                        alert.setContentText("Duplicate name !");
                        alert.showAndWait();
                        check = 1;
                        break;
                    }
                }
            }

            if (check == 0 && flag_check_name == 0 && flag_imp == 1) {
                String type = "project";
                imp = ch.checkImportance(this.p_impTextField.getText());
                projectWork = new ProjectWork(imp, status, type, category_string, name, name_leader, day_start, day_end, type);
                projectWork.setType(type);
                projectWork.setName_work(name);
                System.out.println(projectWork.toString());
                Alert alert = new Alert(AlertType.CONFIRMATION, "Create new Work Of Project ?", new ButtonType[]{ButtonType.OK, ButtonType.CANCEL});
                alert.setTitle("Create new Work of project.");
                Optional<ButtonType> result = alert.showAndWait();
                if (result.get() == ButtonType.OK) {
                    works.add(projectWork);
                    rw.writeWorkDataBase(works);
                    p_nameTextField.clear();
                    p_nameLeaderTextField.clear();
                    p_dayStartTextField.getEditor().clear();
                    p_dayEndTextField.getEditor().clear();
                    p_statusTextField.clear();
                    p_impTextField.clear();
                    initialize();
                }
            }
        }

    }

    @FXML
    public void handleBackButton(ActionEvent actionEvent) {
        try {
            FXRouter.goTo("MenuOfCreating");
        } catch (IOException var3) {
            System.err.println("ไปที่หน้า MenuOfCreating ไม่ได้");
            System.err.println("ให้ตรวจสอบการกำหนด route");
        }

    }

    @FXML
    public void CreateTypeButton(ActionEvent event) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(this.getClass().getResource("/CreatingType.fxml"));
            Scene scene = new Scene((Parent)loader.load());
            Button b = (Button)event.getSource();
            Stage stage = (Stage)b.getScene().getWindow();
            CreatingTypeControllers controller = (CreatingTypeControllers)loader.getController();
            controller.initialize();
            stage.setTitle("Creating Category");
            stage.setScene(scene);
            stage.show();
        } catch (IOException var7) {
            System.err.println("ไปที่หน้า CreatingType ไม่ได้");
            System.err.println("ให้ตรวจสอบการกำหนด route");
        }

    }
}
