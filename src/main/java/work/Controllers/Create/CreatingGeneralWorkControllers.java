package work.Controllers.Create;

import com.github.saacsos.FXRouter;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import work.AllUse.EverythingCheck;
import work.AllUse.RWfile;
import work.Models.GeneralWork;
import work.Models.Type;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;

public class CreatingGeneralWorkControllers {

    @FXML TextField nameTextField;
    @FXML DatePicker dayDatePicker;
    @FXML TextField startTextField;
    @FXML TextField endTextField;
    @FXML TextField statusTextField;
    @FXML TextField impTextField;
    @FXML ComboBox select_category_combobox;
    RWfile rw = new RWfile();
    Alert alert;
    EverythingCheck ch = new EverythingCheck();
    private int index;


    @FXML
    public void initialize() throws IOException {
        ArrayList<Type> types = rw.readTypeDataBase();

        for(int i = 0; i < types.size(); ++i) {
            select_category_combobox.getItems().addAll(new Object[]{(types.get(i)).getNameType()});
        }

        System.out.println("initialize GeneralWorkController");
    }

    @FXML
    public void handleAddConfirmButton(ActionEvent actionEvent) throws IOException {
        String name = nameTextField.getText();
        String day = dayDatePicker.getEditor().getText();
        String imp = impTextField.getText();
        String status = statusTextField.getText();
        String start = startTextField.getText();
        String end = endTextField.getText();
        Object category = select_category_combobox.getValue();
        String category_string = String.valueOf(category);
        ArrayList<GeneralWork> works = rw.readWorkDataBase();
        int flag = 1;
        int flag_check_name = 0;
        int flag_imp = 1;
        System.out.println(category_string);
        if (this.nameTextField.getText().isEmpty()) {
            alert = new Alert(AlertType.ERROR);
            alert.setHeaderText(" ");
            alert.setContentText("Please enter name work");
            alert.showAndWait();
            flag = 0;
        } else if (this.dayDatePicker.getEditor().getText().isEmpty()) {
            alert = new Alert(AlertType.ERROR);
            alert.setHeaderText(" ");
            alert.setContentText("Please enter day at work");
            alert.showAndWait();
        } else if (!ch.checkTime(this.startTextField.getText())) {
            alert = new Alert(AlertType.ERROR);
            alert.setHeaderText(" ");
            alert.setContentText("Please check your start time information !");
            alert.showAndWait();
            System.out.println("ok");
        } else if (!ch.checkTime(this.endTextField.getText())) {
            alert = new Alert(AlertType.ERROR);
            alert.setHeaderText(" ");
            alert.setContentText("Please check your end time information !");
            alert.showAndWait();
        } else if (ch.checkStatusNull(this.statusTextField.getText())) {
            status = ch.checkStatus(this.statusTextField.getText());
            if (status.equals("Invalid Input")) {
                alert = new Alert(AlertType.ERROR);
                alert.setHeaderText(" ");
                alert.setContentText("Please read attention of status");
                alert.showAndWait();
            } else {
                flag = 0;
            }
        } else if (!ch.checkStatusNull(this.statusTextField.getText())) {
            alert = new Alert(AlertType.ERROR);
            alert.setHeaderText(" ");
            alert.setContentText("Please enter status of work");
            alert.showAndWait();
        }

        if (ch.checkImportanceNull(this.impTextField.getText()) && flag != 1) {
            imp = ch.checkImportance(this.impTextField.getText());
            if (imp.equals("Invalid input")) {
                alert = new Alert(AlertType.ERROR);
                alert.setHeaderText(" ");
                alert.setContentText("Please read attention of importance");
                alert.showAndWait();
                System.out.println(imp);
                flag_imp = 0;
            }
            } else if (!ch.checkImportanceNull(this.impTextField.getText())) {
            alert = new Alert(AlertType.ERROR);
            alert.setHeaderText(" ");
            alert.setContentText("Please enter importance of work");
            alert.showAndWait();
            flag_imp = 0;
        }
        if (category_string.equals("null")) {
            alert = new Alert(AlertType.ERROR);
            alert.setHeaderText(" ");
            alert.setContentText("Please select category of work");
            alert.showAndWait();
        } else {
            int check = 0;
            if (flag_check_name == 0 && !ch.checkNameType(this.nameTextField.getText())) {
                alert = new Alert(AlertType.ERROR);
                alert.setHeaderText(" ");
                alert.setContentText("Please check your name work information");
                alert.showAndWait();
                check = 1;
            }

            if (flag_check_name == 0) {
                for(int i = 0; i < works.size(); ++i) {
                    if ((works.get(i)).getName_work().equals(this.nameTextField.getText()) && i != index) {
                        alert = new Alert(AlertType.ERROR);
                        alert.setHeaderText(" ");
                        alert.setContentText("Duplicate name !");
                        alert.showAndWait();
                        check = 1;
                        break;
                    }
                }
            }

            if (check == 0 && flag_check_name == 0 && flag_imp == 1) {
                String type = "general";
                imp = ch.checkImportance(this.impTextField.getText());
                GeneralWork generalWork = new GeneralWork(name, day, imp, status, start, end, type, category_string);
                System.out.println(generalWork.toString());
                Alert alert = new Alert(AlertType.CONFIRMATION, "Create new General Work ?", new ButtonType[]{ButtonType.OK, ButtonType.CANCEL});
                alert.setTitle("Create new General work.");
                Optional<ButtonType> result = alert.showAndWait();
                if (result.get() == ButtonType.OK) {
                    works.add(generalWork);
                    rw.writeWorkDataBase(works);
                    nameTextField.clear();
                    dayDatePicker.getEditor().clear();
                    startTextField.clear();
                    endTextField.clear();
                    statusTextField.clear();
                    impTextField.clear();
                    select_category_combobox.getItems().clear();
                    initialize();
                }
            }
        }

    }

    @FXML
    public void handleBackButton(ActionEvent actionEvent) {
        try {
            FXRouter.goTo("MenuOfCreating");
        } catch (IOException var3) {
            System.err.println("ไปที่หน้า MenuOfCreating ไม่ได้");
            System.err.println("ให้ตรวจสอบการกำหนด route");
        }

    }

    @FXML
    public void CreateTypeButton(ActionEvent event) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/CreatingType.fxml"));
            Scene scene = new Scene(loader.load());
            Button b = (Button)event.getSource();
            Stage stage = (Stage)b.getScene().getWindow();
            CreatingTypeControllers controller = loader.getController();
            controller.initialize();
            stage.setTitle("Creating Category");
            stage.setScene(scene);
            stage.show();
        } catch (IOException var7) {
            System.err.println("ไปที่หน้า CreatingType ไม่ได้");
            System.err.println("ให้ตรวจสอบการกำหนด route");
        }

    }
}
