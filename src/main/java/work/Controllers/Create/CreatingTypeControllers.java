package work.Controllers.Create;

import com.github.saacsos.FXRouter;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import work.Models.Type;
import work.AllUse.EverythingCheck;
import work.AllUse.RWfile;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;

public class CreatingTypeControllers {

    @FXML TextField textFieldType;
    @FXML Label checkType;
    @FXML private ImageView image6;

    private Type type;

    @FXML
    public void initialize() {
        image6.setImage(new Image("/iconCute/6.png"));
        System.out.println("initialize TypeController");
        type = (Type) FXRouter.getData();
    }

    @FXML
    public void handleTypeConfirmButton() throws IOException {

        EverythingCheck check1 = new EverythingCheck();
        RWfile rw = new RWfile();
        ArrayList<Type> typeList = rw.readTypeDataBase();
        Type typeNew = new Type();

        int flag = 0;
        if (check1.checkNameType(textFieldType.getText())){
            for (int i=0;i<typeList.size();i++){
                if (typeList.get(i).getNameType().equals(textFieldType.getText())){
                    checkType.setText("Do not duplicate name, please enter a new name ");
                    flag = 1;
                    break;
                }
                else{
                    checkType.setText("");
                    typeNew.setNameType(textFieldType.getText());
                    flag = 0;
                }
            }
        }
        else{
            flag = 2;
            checkType.setText("Wrong name!");
        }

        if (flag==0){
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Confirm your new category ?",
                    ButtonType.OK,
                    ButtonType.CANCEL);
            alert.setTitle("New category");
            Optional<ButtonType> result = alert.showAndWait();

            if (result.get() == ButtonType.OK) {
                typeList.add(typeNew);
                rw.writeTypeDataBase(typeList);
                checkType.setText("");
            }


        }

    }


    @FXML
    public void backMenuButton(ActionEvent actionEvent) {
        try {
            FXRouter.goTo("MenuOfCreating");
        } catch (IOException e) {
            System.err.println("ไปที่หน้า MenuOfCreating ไม่ได้");
            System.err.println("ให้ตรวจสอบการกำหนด route");
        }

    }
}

