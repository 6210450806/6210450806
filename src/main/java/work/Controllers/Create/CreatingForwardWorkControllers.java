package work.Controllers.Create;

import com.github.saacsos.FXRouter;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import work.AllUse.EverythingCheck;
import work.AllUse.RWfile;
import work.Models.ForwardWork;
import work.Models.GeneralWork;
import work.Models.Type;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;

public class CreatingForwardWorkControllers {

    @FXML TextField f_nameTextField;
    @FXML DatePicker f_dayDatePicker;
    @FXML TextField f_startTextField;
    @FXML TextField f_statusTextField;
    @FXML TextField f_impTextField;
    @FXML TextField f_namePersonTextField;
    @FXML ComboBox select_category_combobox;
    private ForwardWork forwardWork;
    RWfile rw = new RWfile();
    EverythingCheck ch = new EverythingCheck();
    Alert alert;
    private int index;

    public CreatingForwardWorkControllers() {
    }

    @FXML
    public void initialize() throws IOException {
        ArrayList<Type> types = this.rw.readTypeDataBase();

        for(int i = 0; i < types.size(); ++i) {
            this.select_category_combobox.getItems().addAll(new Object[]{(types.get(i)).getNameType()});
        }

        System.out.println("initialize ForwardWorkController");
        this.forwardWork = (ForwardWork)FXRouter.getData();
    }

    @FXML
    public void handleAddConfirmButton(ActionEvent actionEvent) throws IOException {

        String name = f_nameTextField.getText();
        String day = f_dayDatePicker.getEditor().getText();
        String imp = f_impTextField.getText();
        String status = f_statusTextField.getText();
        String start = f_startTextField.getText();
        String name_p = f_namePersonTextField.getText();
        Object category = select_category_combobox.getValue();
        String category_string = String.valueOf(category);
        ArrayList<GeneralWork> works = rw.readWorkDataBase();

        int flag = 1;
        int flag_check_name = 0;
        int flag_imp = 1;

        if (this.f_nameTextField.getText().isEmpty()) {
            alert = new Alert(AlertType.ERROR);
            alert.setHeaderText(" ");
            alert.setContentText("Please enter name work");
            alert.showAndWait();
        } else if (this.f_dayDatePicker.getEditor().getText().isEmpty()) {
            alert = new Alert(AlertType.ERROR);
            alert.setHeaderText(" ");
            alert.setContentText("Please enter day at work");
            alert.showAndWait();
        } else if (this.f_namePersonTextField.getText().isEmpty()) {
            alert = new Alert(AlertType.ERROR);
            alert.setHeaderText(" ");
            alert.setContentText("Please enter name of Responsible person");
            alert.showAndWait();
            System.out.println("ok");
        } else if (this.f_startTextField.getText().isEmpty()) {
            alert = new Alert(AlertType.ERROR);
            alert.setHeaderText(" ");
            alert.setContentText("Please enter time to start at work");
            alert.showAndWait();
            System.out.println("ok");
        } else if (!ch.checkTime(this.f_startTextField.getText())) {
            alert = new Alert(AlertType.ERROR);
            alert.setHeaderText(" ");
            alert.setContentText("Please check your start time information !");
            alert.showAndWait();
        } else if (ch.checkStatusNull(this.f_statusTextField.getText())) {
            status = ch.checkStatus(this.f_statusTextField.getText());
            if (status.equals("Invalid Input")) {
                alert = new Alert(AlertType.ERROR);
                alert.setHeaderText(" ");
                alert.setContentText("Please read attention of status");
                alert.showAndWait();
            } else {
                flag = 0;
            }
        } else if (!ch.checkStatusNull(this.f_statusTextField.getText())) {
            alert = new Alert(AlertType.ERROR);
            alert.setHeaderText(" ");
            alert.setContentText("Please enter status of work");
            alert.showAndWait();
        }

        if (ch.checkImportanceNull(this.f_impTextField.getText()) && flag != 1) {
            imp = ch.checkImportance(this.f_impTextField.getText());
            if (imp.equals("Invalid input")) {
                alert = new Alert(AlertType.ERROR);
                alert.setHeaderText(" ");
                alert.setContentText("Please read attention of importance");
                alert.showAndWait();
                System.out.println(imp);
                flag_imp = 0;
            }
        } else if (!ch.checkImportanceNull(this.f_impTextField.getText())) {
            alert = new Alert(AlertType.ERROR);
            alert.setHeaderText(" ");
            alert.setContentText("Please enter importance of work");
            alert.showAndWait();
            flag_imp = 0;
        }
        if (category_string.equals("null")) {
            alert = new Alert(AlertType.ERROR);
            alert.setHeaderText(" ");
            alert.setContentText("Please select category of work");
            alert.showAndWait();
        } else {
            int check = 0;
            if (flag_check_name == 0 && !ch.checkNameType(this.f_nameTextField.getText())) {
                alert = new Alert(AlertType.ERROR);
                alert.setHeaderText(" ");
                alert.setContentText("Please check your name work information");
                alert.showAndWait();
                check = 1;
            }

            if (flag_check_name == 0) {
                for(int i = 0; i < works.size(); ++i) {
                    if ((works.get(i)).getName_work().equals(this.f_nameTextField.getText()) && i != index) {
                        alert = new Alert(AlertType.ERROR);
                        alert.setHeaderText(" ");
                        alert.setContentText("Duplicate name !");
                        alert.showAndWait();
                        check = 1;
                        break;
                    }
                }
            }

            if (check == 0 && flag_check_name == 0 && flag_imp == 1) {
                String type = "forward";
                imp = ch.checkImportance(this.f_impTextField.getText());
                GeneralWork forward_work_tmp = new ForwardWork(day, imp, status, start, name, name_p, category_string, type);
                System.out.println(forward_work_tmp.toString());
                Alert alert = new Alert(AlertType.CONFIRMATION, "Create new Work Of Forward ?", new ButtonType[]{ButtonType.OK, ButtonType.CANCEL});
                alert.setTitle("Create new Work of forward.");
                Optional<ButtonType> result = alert.showAndWait();
                if (result.get() == ButtonType.OK) {
                    works.add(forward_work_tmp);
                    rw.writeWorkDataBase(works);
                    f_nameTextField.clear();
                    f_dayDatePicker.getEditor().clear();
                    f_impTextField.clear();
                    f_startTextField.clear();
                    f_namePersonTextField.clear();
                    select_category_combobox.getItems().clear();
                    initialize();
                }
            }
        }

    }

    @FXML
    public void handleBackButton(ActionEvent actionEvent) {
        try {
            FXRouter.goTo("MenuOfCreating");
        } catch (IOException var3) {
            System.err.println("ไปที่หน้า MenuOfCreating ไม่ได้");
            System.err.println("ให้ตรวจสอบการกำหนด route");
        }

    }

    @FXML
    public void CreateTypeButton(ActionEvent event) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/CreatingType.fxml"));
            Scene scene = new Scene(loader.load());
            Button b = (Button)event.getSource();
            Stage stage = (Stage)b.getScene().getWindow();
            CreatingTypeControllers controller = (loader.getController());
            controller.initialize();
            stage.setTitle("Creating Category");
            stage.setScene(scene);
            stage.show();
        } catch (IOException var7) {
            System.err.println("ไปที่หน้า CreatingType ไม่ได้");
            System.err.println("ให้ตรวจสอบการกำหนด route");
        }

    }
}
