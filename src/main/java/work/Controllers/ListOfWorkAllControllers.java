package work.Controllers;

import com.github.saacsos.FXRouter;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import work.AllUse.EverythingCheck;
import work.AllUse.RWfile;
import work.Models.*;
import work.Controllers.Edit.EditForwardWorkControllers;
import work.Controllers.Edit.EditGeneralWorkControllers;
import work.Controllers.Edit.EditProjectWorkControllers;
import work.Controllers.Edit.EditWeekWorkControllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;

public class ListOfWorkAllControllers {

    @FXML ListView listview;
    @FXML ComboBox category_combobox;
    @FXML ComboBox status_combobox;
    @FXML DatePicker date_daypicker;
    @FXML Label value_label;
    @FXML Label special1;
    @FXML Label special2;
    @FXML Label special3;
    @FXML Label special4;
    @FXML Label value1;
    @FXML Label value2;
    RWfile rw = new RWfile();
    EverythingCheck ic = new EverythingCheck();


    public String changeType(String t) {
        String type = "";
        if (t.equals("general")) {
            type = type + "General work";
        } else if (t.equals("week")) {
            type = type + "Work of week";
        } else if (t.equals("project")) {
            type = type + "Project work";
        } else if (t.equals("forward")) {
            type = type + "Forward work";
        }

        return type;
    }

    public void initializeText() throws IOException {
        ArrayList<Type> types = rw.readTypeDataBase();

        for(int i = 0; i < types.size(); ++i) {
            category_combobox.getItems().addAll(new Object[]{((Type)types.get(i)).getNameType()});
        }

        status_combobox.getItems().addAll(new Object[]{"Not started yet"});
        status_combobox.getItems().addAll(new Object[]{"Working"});
        status_combobox.getItems().addAll(new Object[]{"Completed"});
    }

    public void initializeList() throws IOException {
        ArrayList<GeneralWork> works = rw.readWorkDataBase();

        for(int i = 0; i < works.size(); ++i) {
            if ((works.get(i)).getImportance().equals("Great")) {
                String type = (works.get(i)).getType();
                String name_work = (works.get(i)).getName_work();
                String imp = (works.get(i)).getImportance();
                String status = (works.get(i)).getStatus();
                type = changeType(type);
                listview.getItems().add("Name of work : " + name_work + "\nType of work : " + type + "\nImportance : " + imp + "\nStatus : " + status + "\n" +"_________________________________________________________________________________________________________--" + "\n");
            }
        }

        for(int i = 0; i < works.size(); ++i) {
            if ((works.get(i)).getImportance().equals("Medium")) {
                String type = (works.get(i)).getType();
                String name_work = (works.get(i)).getName_work();
                String imp = (works.get(i)).getImportance();
                String status = (works.get(i)).getStatus();
                type = changeType(type);
                listview.getItems().add("Name of work : " + name_work + "\nType of work : " + type + "\nImportance : " + imp + "\nStatus : " + status + "\n" + "_________________________________________________________________________________________________________" + "\n");
            }
        }

        for(int i = 0; i < works.size(); ++i) {
            if ((works.get(i)).getImportance().equals("Little")) {
                String type = (works.get(i)).getType();
                String name_work = (works.get(i)).getName_work();
                String imp = (works.get(i)).getImportance();
                String status = (works.get(i)).getStatus();
                type = changeType(type);
                listview.getItems().add("Name of work : " + name_work + "\nType of work : " + type + "\nImportance : " + imp + "\nStatus : " + status + "\n" +"_________________________________________________________________________________________________________" + "\n");
            }
        }

    }

    public void showAction() throws IOException {
        special1.setText("");
        special2.setText("");
        special3.setText("");
        special4.setText("");
        listview.getItems().clear();
        //ArrayList<Type> types = rw.readTypeDataBase();
        ArrayList<GeneralWork> works = rw.readWorkDataBase();
        Object category = category_combobox.getValue();
        String category_string = String.valueOf(category);
        Object status = status_combobox.getValue();
        String status_string = String.valueOf(status);
        String day = date_daypicker.getEditor().getText();
        String category_tmp = "";
        int countcate = 0;


        if (!category_string.equals("null") && status_string.equals("null") && day.equals("")) {
            for(int i = 0; i < works.size(); ++i) {
                if ((works.get(i)).getType_category().equals(category_string)) {
                    String type = (works.get(i)).getType();
                    String namework = (works.get(i)).getName_work();
                    String imp = (works.get(i)).getImportance();
                    status = (works.get(i)).getStatus();
                    category_tmp = (works.get(i)).getType_category();
                    type = changeType(type);
                    listview.getItems().add("Name of work : " + namework + "\nType of work : " + type + "\nImportance : " + imp + "\nStatus : " + status + "\n" +"_________________________________________________________________________________________________________" + "\n");
                }
            }
        } else if (category_string.equals("null") && !status_string.equals("null") && day.equals("")) {
            for(int i = 0; i < works.size(); ++i) {
                if ((works.get(i)).getStatus().equals(status_string)) {
                    String type = (works.get(i)).getType();
                    String namework = (works.get(i)).getName_work();
                    String imp = (works.get(i)).getImportance();
                    status = (works.get(i)).getStatus();
                    type = changeType(type);
                    listview.getItems().add("Name of work : " + namework + "\nType of work : " + type + "\nImportance : " + imp + "\nStatus : " + status + "\n" + "_________________________________________________________________________________________________________" + "\n");
                }
            }
        } else if (category_string.equals("null") && status_string.equals("null") && !day.equals("")) {
            for(int i = 0; i < works.size(); ++i) {
                if ((works.get(i)).getDay().equals(day)) {
                    String type = (works.get(i)).getType();
                    String namework = (works.get(i)).getName_work();
                    String imp = (works.get(i)).getImportance();
                    status = (works.get(i)).getStatus();
                    type = changeType(type);
                    listview.getItems().add("Name of work : " + namework + "\nType of work : " + type + "\nImportance : " + imp + "\nStatus : " + status + "\n" + "_________________________________________________________________________________________________________" + "\n");
                }
            }
        } else if (!category_string.equals("null") && !status_string.equals("null") && day.equals("")) {
            for(int i = 0; i < works.size(); ++i) {
                if ((works.get(i)).getType_category().equals(category_string) && (works.get(i)).getStatus().equals(status_string)) {
                    String type = (works.get(i)).getType();
                    String namework = (works.get(i)).getName_work();
                    String imp = (works.get(i)).getImportance();
                    status = (works.get(i)).getStatus();
                    category_tmp = (works.get(i)).getType_category();
                    type = changeType(type);
                    listview.getItems().add("Name of work : " + namework + "\nType of work : " + type + "\nImportance : " + imp + "\nStatus : " + status + "\n" + "_________________________________________________________________________________________________________" + "\n");
                }
            }
        } else if (category_string.equals("null") && !status_string.equals("null") && !day.equals("")) {
            for(int i = 0; i < works.size(); ++i) {
                if ((works.get(i)).getDay().equals(day) && (works.get(i)).getStatus().equals(status_string)) {
                    String type = (works.get(i)).getType();
                    String namework = (works.get(i)).getName_work();
                    String imp = (works.get(i)).getImportance();
                    status = (works.get(i)).getStatus();
                    type = changeType(type);
                    listview.getItems().add("Name of work : " + namework + "\nType of work : " + type + "\nImportance : " + imp + "\nStatus : " + status + "\n" + "_________________________________________________________________________________________________________" + "\n");
                }
            }
        } else if (!category_string.equals("null") && status_string.equals("null") && !day.equals("")) {
            for(int i = 0; i < works.size(); ++i) {
                if ((works.get(i)).getDay().equals(day) && (works.get(i)).getType_category().equals(category_string)) {
                    String type = (works.get(i)).getType();
                    String namework = (works.get(i)).getName_work();
                    String imp = (works.get(i)).getImportance();
                    status = (works.get(i)).getStatus();
                    category_tmp = (works.get(i)).getType_category();
                    type = changeType(type);
                    listview.getItems().add("Name of work : " + namework + "\nType of work : " + type + "\nImportance : " + imp + "\nStatus : " + status + "\n" + "_________________________________________________________________________________________________________" + "\n");
                }
            }
        } else if (!category_string.equals("null") && !status_string.equals("null") && !day.equals("")) {
            for(int i = 0; i < works.size(); ++i) {
                if ((works.get(i)).getDay().equals(day) && (works.get(i)).getStatus().equals(status_string) && (works.get(i)).getType_category().equals(category_string)) {
                    String type = (works.get(i)).getType();
                    String namework = (works.get(i)).getName_work();
                    String imp = (works.get(i)).getImportance();
                    status = (works.get(i)).getStatus();
                    category_tmp = (works.get(i)).getType_category();
                    type = changeType(type);
                    listview.getItems().add("Name of work : " + namework + "\nType of work : " + type + "\nImportance : " + imp + "\nStatus : " + status + "\n" + "_________________________________________________________________________________________________________" + "\n");
                }
            }
        } else {
            initializeList();
        }

        if (!category_string.equals("null")) {
            int count_general = ic.countTypeCategoryInWork(works,category_string,"general");
            int count_week = ic.countTypeCategoryInWork(works,category_string,"week");
            int count_forward = ic.countTypeCategoryInWork(works,category_string,"forward");
            int count_project = ic.countTypeCategoryInWork(works,category_string,"project");
            countcate = ic.countCategory(category_string, works);
            value_label.setText(category_tmp + " have : " + countcate);
            value1.setText("General work : " + count_general + " / Work of week : " + count_week);
            value2.setText("Forward work : " + count_forward + " / Project work : " + count_project);

        } else {
            value_label.setText("");
        }

        //count_general = ic.countGeneralWork(works,types);


        status_combobox.getItems().clear();
        category_combobox.getItems().clear();
        date_daypicker.getEditor().clear();
        initializeText();
    }

    public void informationAction() throws IOException {
        if (listview.getSelectionModel().getSelectedItem() != null) {
            ArrayList<GeneralWork> works = rw.readWorkDataBase();
            String text = listview.getSelectionModel().getSelectedItem().toString();
            String name_work_tmp = text.split("\n")[0];
            String name_work = name_work_tmp.split(":")[1].trim();

            for(int i = 0; i < works.size(); ++i) {
                if (name_work.equals((works.get(i)).getName_work())) {
                    String sp1;
                    String sp2;
                    String sp3;
                    String sp4;
                    if ((works.get(i)).getType().equals("general")) {
                        sp1 = (works.get(i)).getDay();
                        sp2 = (works.get(i)).getStart();
                        sp3 = (works.get(i)).getEnd();
                        sp4 = (works.get(i)).getType_category();
                        special1.setText("Day : " + sp1);
                        special2.setText("Start time : " + sp2);
                        special3.setText("End time : " + sp3);
                        special4.setText("Category : " + sp4);
                    }

                    if ((works.get(i)).getType().equals("week")) {
                        WeekWork work_tmp = (WeekWork)works.get(i);
                        sp1 = work_tmp.getWday();
                        sp2 = work_tmp.getWstart();
                        sp3 = work_tmp.getWend();
                        sp4 = work_tmp.getType_category();
                        special1.setText("Day of week : " + sp1);
                        special2.setText("Start time : " + sp2);
                        special3.setText("End time : " + sp3);
                        special4.setText("Category : " + sp4);
                    }

                    if ((works.get(i)).getType().equals("forward")) {
                        ForwardWork work_tmp = (ForwardWork)works.get(i);
                        sp1 = work_tmp.getForward_name();
                        sp2 = work_tmp.getDay();
                        sp3 = work_tmp.getStart();
                        sp4 = work_tmp.getType_category();
                        special1.setText("Forward name : " + sp1);
                        special2.setText("Day : " + sp2);
                        special3.setText("Time : " + sp3);
                        special4.setText("Category : " + sp4);
                    }

                    if ((works.get(i)).getType().equals("project")) {
                        ProjectWork work_tmp = (ProjectWork)works.get(i);
                        sp1 = work_tmp.getName_leader();
                        sp2 = work_tmp.getDay_start();
                        sp3 = work_tmp.getDay_end();
                        sp4 = work_tmp.getType_category();
                        special1.setText("Leader name : " + sp1);
                        special2.setText("Start date : " + sp2);
                        special3.setText("End date : " + sp3);
                        special4.setText("Category : " + sp4);
                    }
                }
            }
        }

    }

    public void editAction(ActionEvent event) throws IOException {
        if (listview.getSelectionModel().getSelectedItem() != null) {
            ArrayList<GeneralWork> works = rw.readWorkDataBase();
            String text = listview.getSelectionModel().getSelectedItem().toString();
            String name_work_tmp = text.split("\n")[0];
            String name_work = name_work_tmp.split(":")[1].trim();
            String type = "";
            int index = 0;

            for(int i = 0; i < works.size(); ++i) {
                if (name_work.equals((works.get(i)).getName_work())) {
                    type = (works.get(i)).getType();
                    index = i;
                }
            }

            Alert alert = new Alert(AlertType.CONFIRMATION, "Edit Work ?", new ButtonType[]{ButtonType.OK, ButtonType.CANCEL});
            alert.setTitle("Edit work ?");
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                FXMLLoader loader;
                Scene scene;
                Button b;
                Stage stage;
                if (type.equals("general")) {
                    loader = new FXMLLoader();
                    loader.setLocation(getClass().getResource("/EditGeneralWork.fxml"));
                    scene = new Scene(loader.load());
                    b = (Button)event.getSource();
                    stage = (Stage)b.getScene().getWindow();
                    EditGeneralWorkControllers controller = loader.getController();
                    controller.setWorkTmp(works.get(index));
                    controller.setIndex(index);
                    controller.initializeText();
                    stage.setTitle("Edit General work");
                    stage.setScene(scene);
                    stage.show();
                } else if (type.equals("week")) {
                    loader = new FXMLLoader();
                    loader.setLocation(getClass().getResource("/EditWeekWork.fxml"));
                    scene = new Scene(loader.load());
                    b = (Button)event.getSource();
                    stage = (Stage)b.getScene().getWindow();
                    EditWeekWorkControllers controller = loader.getController();
                    controller.setWorkTmp((WeekWork)works.get(index));
                    controller.setIndex(index);
                    controller.initializeText();
                    stage.setTitle("Edit work of week");
                    stage.setScene(scene);
                    stage.show();
                } else if (type.equals("project")) {
                    loader = new FXMLLoader();
                    loader.setLocation(getClass().getResource("/EditProjectWork.fxml"));
                    scene = new Scene(loader.load());
                    b = (Button)event.getSource();
                    stage = (Stage)b.getScene().getWindow();
                    EditProjectWorkControllers controller = loader.getController();
                    controller.setWorkTmp((ProjectWork)works.get(index));
                    controller.setIndex(index);
                    controller.initializeText();
                    stage.setTitle("Edit project work");
                    stage.setScene(scene);
                    stage.show();
                } else if (type.equals("forward")) {
                    loader = new FXMLLoader();
                    loader.setLocation(getClass().getResource("/EditForwardWork.fxml"));
                    scene = new Scene(loader.load());
                    b = (Button)event.getSource();
                    stage = (Stage)b.getScene().getWindow();
                    EditForwardWorkControllers controller = loader.getController();
                    controller.setWorkTmp((ForwardWork)works.get(index));
                    controller.setIndex(index);
                    controller.initializeText();
                    stage.setTitle("Edit project work");
                    stage.setScene(scene);
                    stage.show();
                }
            }
        }

    }

    @FXML
    public void menuAllButton(ActionEvent actionEvent) {
        try {
            FXRouter.goTo("MenuAll");
        } catch (IOException var3) {
            System.err.println("ไปที่หน้า MenuAll ไม่ได้");
            System.err.println("ให้ตรวจสอบการกำหนด route");
        }

    }
}
