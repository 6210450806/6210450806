package work.Controllers;

import com.github.saacsos.FXRouter;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.io.IOException;

public class HomeControllers {

    @FXML private ImageView image1;
    @FXML private ImageView image2;
    @FXML private ImageView image3;

    @FXML
    public void initialize() {
        image1.setImage(new Image("/iconCute/1.png"));
        image2.setImage(new Image("/iconCute/2.png"));
        image3.setImage(new Image("/iconCute/3.png"));

    }

    @FXML
    public void adminButton(ActionEvent actionEvent) {
        try {
            FXRouter.goTo("Producer");
        } catch (IOException e) {
            System.err.println("ไปที่หน้า Producer ไม่ได้");
            System.err.println("ให้ตรวจสอบการกำหนด route");
        }
    }

    @FXML
    public void menuAllButton(ActionEvent actionEvent) {
        try {
            FXRouter.goTo("MenuAll");
        } catch (IOException e) {
            System.err.println("ไปที่หน้า MenuAll ไม่ได้");
            System.err.println("ให้ตรวจสอบการกำหนด route");
        }
    }

    @FXML
    public void manualButton(ActionEvent actionEvent) {
        try {
            FXRouter.goTo("Instruction");
        } catch (IOException e) {
            System.err.println("ไปที่หน้า Instruction ไม่ได้");
            System.err.println("ให้ตรวจสอบการกำหนด route");
        }
    }



}
