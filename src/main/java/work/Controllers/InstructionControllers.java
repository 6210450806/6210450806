package work.Controllers;

import com.github.saacsos.FXRouter;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;

import java.io.IOException;

public class InstructionControllers {

    @FXML
    public void backHomeButton(ActionEvent actionEvent) {
        try {
            FXRouter.goTo("Home");
        } catch (IOException e) {
            System.err.println("ไปที่หน้า Home ไม่ได้");
            System.err.println("ให้ตรวจสอบการกำหนด route");
        }
    }
}
