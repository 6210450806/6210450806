package work.Controllers;
import com.github.saacsos.FXRouter;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.io.IOException;

public class ProducerControllers {

    @FXML
    private ImageView image;

    @FXML
    public void initialize() {
        image.setImage(new Image("/natthapat.jpg"));

    }

    @FXML
    public void backHomeButton(ActionEvent actionEvent) {
        try {
            FXRouter.goTo("Home");
        } catch (IOException e) {
            System.err.println("ไปที่หน้า Home ไม่ได้");
            System.err.println("ให้ตรวจสอบการกำหนด route");
        }

    }


}
